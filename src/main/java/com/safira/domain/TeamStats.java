package com.safira.domain;

import com.safira.domain.entities.Team;

public class TeamStats {

    private Team team;
    private Integer points;
    private Integer goalDifference;
    private Integer goalsFor;

    public TeamStats() {
    }

    public TeamStats(Team team, Integer points, Integer goalDifference, Integer goalsFor) {
        this.team = team;
        this.points = points;
        this.goalDifference = goalDifference;
        this.goalsFor = goalsFor;
    }

    public TeamStats(Team thirdPlace, Integer[] thirdPlaceStats) {
        this.team = thirdPlace;
        this.points = thirdPlaceStats[0];
        this.goalsFor = thirdPlaceStats[1];
        this.goalDifference = thirdPlaceStats[2];
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(Integer goalDifference) {
        this.goalDifference = goalDifference;
    }

    public Integer getGoalsFor() {
        return goalsFor;
    }

    public void setGoalsFor(Integer goalsFor) {
        this.goalsFor = goalsFor;
    }
}
