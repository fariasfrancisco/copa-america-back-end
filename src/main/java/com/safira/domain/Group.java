package com.safira.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.safira.api.requests.MatchResponses;
import com.safira.api.requests.ThirdPlaceTeams;
import com.safira.api.responses.MatchResponse;
import com.safira.domain.entities.Match;
import com.safira.domain.entities.Team;

import java.util.*;

public class Group {

    private Team firstPlace;

    private Team secondPlace;

    private Team thirdPlace;

    private Team fourthPlace;

    @JsonIgnore
    private Team[] teams;
    @JsonIgnore
    private Integer[] points;
    @JsonIgnore
    private Integer[] goalsFor;
    @JsonIgnore
    private Integer[] goalDifference;

    public Group(ThirdPlaceTeams teams) {
        int length = teams.getTeamStatsList().size();
        this.teams = new Team[length];
        this.points = new Integer[length];
        this.goalsFor = new Integer[length];
        this.goalDifference = new Integer[length];

        for (int index = 0; index < length; index++) {
            TeamStats teamStats = teams.getTeamStatsList().get(index);
            this.teams[index] = teamStats.getTeam();
            this.points[index] = teamStats.getPoints();
            this.goalsFor[index] = teamStats.getGoalsFor();
            this.goalDifference[index] = teamStats.getGoalDifference();
        }

        rank();
    }

    public Group(MatchResponses responses, Team[] teams) {
        this.teams = teams;
        InitializePointsAndGoals();
        loadResults(responses);
        rank();
    }

    public Group(List<Match> matches) {
        List<MatchResponse> responseList = new ArrayList<>();
        for (Match match : matches) responseList.add(new MatchResponse(match));
        MatchResponses responses = new MatchResponses(responseList);

        loadTeams(matches);
        InitializePointsAndGoals();
        loadResults(responses);
        rank();
    }

    public Group() {
    }

    public Team getFirstPlace() {
        return firstPlace;
    }

    public void setFirstPlace(Team firstPlace) {
        this.firstPlace = firstPlace;
    }

    public Team getSecondPlace() {
        return secondPlace;
    }

    public void setSecondPlace(Team secondPlace) {
        this.secondPlace = secondPlace;
    }

    public Team getThirdPlace() {
        return thirdPlace;
    }

    public void setThirdPlace(Team thirdPlace) {
        this.thirdPlace = thirdPlace;
    }

    public Team getFourthPlace() {
        return fourthPlace;
    }

    public void setFourthPlace(Team fourthPlace) {
        this.fourthPlace = fourthPlace;
    }

    @JsonIgnore
    public Integer[] getThirdPlaceStats() {
        return new Integer[]{points[2], goalsFor[2], goalDifference[2]};
    }

    private void loadResults(MatchResponses matchResponses) {
        int length = teams.length;
        for (MatchResponse match : matchResponses.getMatchResponses()) {
            Integer homeIdx = 0;
            Integer awayIdx = 0;

            for (int index = 0; index < length; index++) {
                String name = teams[index].getName();
                if (name.equals(match.getHomeTeam())) homeIdx = index;
                if (name.equals(match.getAwayTeam())) awayIdx = index;
            }

            Integer goalsHome = match.getHomeGoals();
            Integer goalsAway = match.getAwayGoals();
            int homeDiff = goalsHome - goalsAway;
            int awayDiff = goalsAway - goalsHome;

            this.goalsFor[homeIdx] += goalsHome;
            this.goalsFor[awayIdx] += goalsAway;
            this.goalDifference[homeIdx] += homeDiff;
            this.goalDifference[awayIdx] += awayDiff;

            int winner = match.calculateWinner();

            if (winner > 0) this.points[homeIdx] += 3;
            else if (winner < 0) this.points[awayIdx] += 3;
            else {
                this.points[homeIdx] += 1;
                this.points[awayIdx] += 1;
            }
        }
    }

    private void loadTeams(List<Match> matches) {
        List<Team> teamList = new ArrayList<>();
        for (Match match : matches) {
            if (!teamList.contains(match.getHomeTeam())) teamList.add(match.getHomeTeam());
            if (!teamList.contains(match.getAwayTeam())) teamList.add(match.getAwayTeam());
        }

        Map<String, Team> map = new LinkedHashMap<>();
        for (Team team : teamList) map.put(team.getName(), team);

        teamList.clear();
        teamList.addAll(map.values());

        int length = teamList.size();
        this.teams = new Team[length];
        for (int i = 0; i < length; i++) this.teams[i] = teamList.get(i);
    }

    private void InitializePointsAndGoals() {
        int length = teams.length;

        this.points = new Integer[length];
        this.goalsFor = new Integer[length];
        this.goalDifference = new Integer[length];

        Arrays.fill(this.points, 0);
        Arrays.fill(this.goalsFor, 0);
        Arrays.fill(this.goalDifference, 0);
    }

    private void rank() {
        int length = teams.length;
        Map<Team, Integer> pts = new HashMap<>();
        Map<Team, Integer> gf = new HashMap<>();
        Map<Team, Integer> gd = new HashMap<>();
        LoadMap(pts, points);
        LoadMap(gf, goalsFor);
        LoadMap(gd, goalDifference);

        boolean swapped = true;
        int j = 0;
        Team tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < teams.length - j; i++) {
                int comparison = Team.compareTeams(teams[i], teams[i + 1], pts, gf, gd);
                if (comparison < 0) {
                    tmp = teams[i];
                    teams[i] = teams[i + 1];
                    teams[i + 1] = tmp;
                    swapped = true;
                }
            }
        }

        for (int index = 0; index < length; index++) {
            this.points[index] = pts.get(teams[index]);
            this.goalsFor[index] = gf.get(teams[index]);
            this.goalDifference[index] = gd.get(teams[index]);
        }

        this.firstPlace = 0 < length ? teams[0] : null;
        this.secondPlace = 1 < length ? teams[1] : null;
        this.thirdPlace = 2 < length ? teams[2] : null;
        this.fourthPlace = 3 < length ? teams[3] : null;
    }

    private void LoadMap(Map<Team, Integer> map, Integer[] array) {
        int length = teams.length;
        for (int index = 0; index < length; index++) map.put(teams[index], array[index]);
    }
}
