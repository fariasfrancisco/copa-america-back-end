package com.safira.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Stage extends AbstractPersistable<Long> {

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, unique = true)
    private StageName stageName;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "stage")
    private List<Match> matches = new ArrayList<>();

    public Stage() {
    }

    public Stage(StageName stageName, List<Match> matches) {
        this.stageName = stageName;
        this.matches = matches;
    }

    public Stage(Builder builder) {
        this.stageName = builder.stageName;
        this.matches = builder.matches;
    }

    @JsonIgnore
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public boolean isNew() {
        return super.isNew();
    }

    public StageName getStageName() {
        return stageName;
    }

    public void setStageName(StageName stageName) {
        this.stageName = stageName;
    }

    public List<Match> getMatches() {
        return matches;
    }

    public void setMatches(List<Match> matches) {
        this.matches = matches;
    }

    public enum StageName {
        GROUP_A, GROUP_B, GROUP_C, QUARTER_FINALS, SEMI_FINALS, FINALS
    }

    public static class Builder {
        private StageName stageName;
        private List<Match> matches = new ArrayList<>();

        public Builder withStageName(StageName stageName) {
            this.stageName = stageName;
            return this;
        }

        public Builder withMatches(List<Match> matches) {
            this.matches = matches;
            return this;
        }

        public Stage build() {
            return new Stage(this);
        }
    }
}
