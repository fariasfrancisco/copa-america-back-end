package com.safira.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;

@Entity
public class BetAccount extends AbstractPersistable<Long> {
    @OneToOne(cascade = CascadeType.ALL, optional = true, fetch = FetchType.EAGER)
    @JoinColumn(name = "bet_id", unique = true)
    private Bet bet;

    @Column(nullable = false, unique = true)
    private String username;

    @JsonIgnore
    private byte[] hash;

    @JsonIgnore
    private byte[] salt;

    private boolean validated;

    public BetAccount() {
    }

    public BetAccount(Bet bet, String username, byte[] hash, byte[] salt) {
        this.bet = bet;
        this.username = username;
        this.hash = hash;
        this.salt = salt;
    }

    public BetAccount(Builder builder) {
        this.username = builder.username;
        this.hash = builder.hash;
        this.salt = builder.salt;
    }

    @JsonIgnore
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public boolean isNew() {
        return super.isNew();
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getHash() {
        return hash;
    }

    public void setHash(byte[] hash) {
        this.hash = hash;
    }

    public byte[] getSalt() {
        return salt;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }

    public boolean isValidated() {
        return validated;
    }

    public void setValidated(boolean validated) {
        this.validated = validated;
    }

    public static class Builder {
        private String username;
        private byte[] hash;
        private byte[] salt;

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withHash(byte[] hash) {
            this.hash = hash;
            return this;
        }

        public Builder withSalt(byte[] salt) {
            this.salt = salt;
            return this;
        }

        public BetAccount build() {
            return new BetAccount(this);
        }
    }
}
