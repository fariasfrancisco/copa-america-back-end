package com.safira.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Goal extends AbstractPersistable<Long> {

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "player_id", nullable = false)
    private Player player;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "match_id", nullable = false)
    private Match match;

    public Goal() {
    }

    public Goal(Player player, Match match) {
        this.player = player;
        this.match = match;
    }

    public Goal(Builder builder) {
        this.player = builder.player;
        this.match = builder.match;
        if (match != null && !match.getGoals().contains(this)) match.getGoals().add(this);
    }

    @JsonIgnore
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public boolean isNew() {
        return super.isNew();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public static class Builder {
        private Player player;
        private Match match;

        public Builder withPlayer(Player player) {
            this.player = player;
            return this;
        }

        public Builder withMatch(Match match) {
            this.match = match;
            return this;
        }

        public Goal build() {
            return new Goal(this);
        }
    }
}
