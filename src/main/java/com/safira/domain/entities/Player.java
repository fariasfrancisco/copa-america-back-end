package com.safira.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Player extends AbstractPersistable<Long> {

    @Column(nullable = false)
    private String fullName;

    @JsonManagedReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "team_id", nullable = false)
    private Team team;

    public Player() {
    }

    public Player(String fullName, Set<Goal> goals, Team team) {
        this.fullName = fullName;
        this.team = team;
    }

    public Player(Builder builder) {
        this.fullName = builder.fullName;
        this.team = builder.team;
        if (!team.getPlayers().contains(this)) team.getPlayers().add(this);
    }

    @JsonIgnore
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public boolean isNew() {
        return super.isNew();
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public static class Builder {
        private String fullName;
        private Team team;

        public Builder withFullName(String fullName) {
            this.fullName = fullName;
            return this;
        }

        public Builder withTeam(Team team) {
            this.team = team;
            return this;
        }

        public Player build() {
            return new Player(this);
        }
    }
}
