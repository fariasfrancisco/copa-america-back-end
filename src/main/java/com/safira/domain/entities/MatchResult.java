package com.safira.domain.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "bet_match")
@AssociationOverrides({
        @AssociationOverride(name = "pk.match", joinColumns = @JoinColumn(name = "match_id")),
        @AssociationOverride(name = "pk.bet", joinColumns = @JoinColumn(name = "bet_id"))})
public class MatchResult implements Serializable {
    @EmbeddedId
    private MatchResultId pk = new MatchResultId();
    private Integer homeGoals;
    private Integer awayGoals;
    private Integer homePenalties;
    private Integer awayPenalties;

    public MatchResult() {
    }

    public MatchResult(Match match,
                       Bet bet,
                       Integer homeGoals,
                       Integer awayGoals,
                       Integer homePenalties,
                       Integer awayPenalties) {
        this.pk.setBet(bet);
        this.pk.setMatch(match);
        this.homeGoals = homeGoals;
        this.awayGoals = awayGoals;
        this.homePenalties = homePenalties;
        this.awayPenalties = awayPenalties;
        if (!bet.getMatchResults().contains(this)) bet.getMatchResults().add(this);
    }

    public MatchResultId getPk() {
        return pk;
    }

    public void setPk(MatchResultId pk) {
        this.pk = pk;
    }

    public Match getMatch() {
        return pk.getMatch();
    }

    public void setMatch(Match match) {
        pk.setMatch(match);
    }

    @JsonBackReference
    public Bet getBet() {
        return pk.getBet();
    }

    public void setBet(Bet bet) {
        pk.setBet(bet);
    }

    public Integer getHomeGoals() {
        return homeGoals;
    }

    public void setHomeGoals(Integer homeGoals) {
        this.homeGoals = homeGoals;
    }

    public Integer getAwayGoals() {
        return awayGoals;
    }

    public void setAwayGoals(Integer awayGoals) {
        this.awayGoals = awayGoals;
    }

    public Integer getHomePenalties() {
        return homePenalties;
    }

    public void setHomePenalties(Integer homePenalties) {
        this.homePenalties = homePenalties;
    }

    public Integer getAwayPenalties() {
        return awayPenalties;
    }

    public void setAwayPenalties(Integer awayPenalties) {
        this.awayPenalties = awayPenalties;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatchResult that = (MatchResult) o;

        if (!pk.equals(that.pk)) return false;
        if (!homeGoals.equals(that.homeGoals)) return false;
        return awayGoals.equals(that.awayGoals);

    }

    @Override
    public int hashCode() {
        int result = pk.hashCode();
        result = 31 * result + homeGoals.hashCode();
        result = 31 * result + awayGoals.hashCode();
        return result;
    }
}
