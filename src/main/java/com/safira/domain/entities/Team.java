package com.safira.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
public class Team extends AbstractPersistable<Long> {

    @JsonBackReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "team")
    private List<Player> players = new ArrayList<>();

    @Column(nullable = false, unique = true)
    private String name;

    public Team() {
    }

    public Team(List<Player> players, String name) {
        this.players = players;
        this.name = name;
    }

    public Team(Builder builder) {
        this.players = builder.players;
        for (Player player : players) if (player.getTeam() != this) player.setTeam(this);
        this.name = builder.name;
    }

    @JsonIgnore
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public boolean isNew() {
        return super.isNew();
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Builder {
        private List<Player> players = new ArrayList<>();
        private String name;

        public Builder withPlayers(List<Player> players) {
            this.players = players;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Team build() {
            return new Team(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Team team = (Team) o;

        return name.equals(team.name);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Team{" +
                "name='" + name + '\'' +
                '}';
    }


    public static int compareTeams(Team a,
                                   Team b,
                                   Map<Team, Integer> points,
                                   Map<Team, Integer> goalsFor,
                                   Map<Team, Integer> goalDifference) {
        int pointDiff = points.get(a) - points.get(b);
        int gfDiff = goalsFor.get(a) - goalsFor.get(b);

        //The following comparison is done because goal differences can be negative
        int gdDiff;
        if (goalDifference.get(a) > goalDifference.get(b)) gdDiff = 1;
        else if (goalDifference.get(a) > goalDifference.get(b)) gdDiff = -1;
        else gdDiff = 0;

        if (pointDiff > 0) return 1;
        else if (pointDiff < 0) return -1;
        else if (gdDiff > 0) return 1;
        else if (gdDiff < 0) return -1;
        else if (gfDiff > 0) return 1;
        else if (gfDiff < 0) return -1;
        else return 0;
    }
}
