package com.safira.domain.entities;

import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class MatchResultId implements Serializable {

    @ManyToOne
    private Match match;

    @ManyToOne
    private Bet bet;

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Bet getBet() {
        return bet;
    }

    public void setBet(Bet bet) {
        this.bet = bet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MatchResultId that = (MatchResultId) o;

        if (!match.equals(that.match)) return false;
        return bet.equals(that.bet);

    }

    @Override
    public int hashCode() {
        int result = match.hashCode();
        result = 31 * result + bet.hashCode();
        return result;
    }
}
