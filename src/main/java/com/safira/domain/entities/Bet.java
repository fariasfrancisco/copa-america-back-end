package com.safira.domain.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Bet extends AbstractPersistable<Long> {

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.bet", cascade = CascadeType.ALL)
    private List<MatchResult> matchResults = new ArrayList<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "goldenBootId", nullable = false)
    private Player goldenBoot;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "firstPlaceId", nullable = false)
    private Team firstPlace;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "secondPlaceId", nullable = false)
    private Team secondPlace;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "thirdPlaceId", nullable = false)
    private Team thirdPlace;

    public Bet() {
    }

    public Bet(String username,
               byte[] hash,
               byte[] salt,
               List<MatchResult> matchResults,
               Player goldenBoot,
               Team firstPlace,
               Team secondPlace,
               Team thirdPlace) {
        this.matchResults = matchResults;
        this.goldenBoot = goldenBoot;
        this.firstPlace = firstPlace;
        this.secondPlace = secondPlace;
        this.thirdPlace = thirdPlace;
    }

    public Bet(Builder builder) {
        this.matchResults = builder.matchResults;
        this.goldenBoot = builder.goldenBoot;
        this.firstPlace = builder.firstPlace;
        this.secondPlace = builder.secondPlace;
        this.thirdPlace = builder.thirdPlace;
    }

    @JsonIgnore
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public boolean isNew() {
        return super.isNew();
    }

    public List<MatchResult> getMatchResults() {
        return matchResults;
    }

    public void setMatchResults(List<MatchResult> matchResults) {
        this.matchResults = matchResults;
    }

    public Player getGoldenBoot() {
        return goldenBoot;
    }

    public void setGoldenBoot(Player goldenBoot) {
        this.goldenBoot = goldenBoot;
    }

    public Team getFirstPlace() {
        return firstPlace;
    }

    public void setFirstPlace(Team firstPlace) {
        this.firstPlace = firstPlace;
    }

    public Team getSecondPlace() {
        return secondPlace;
    }

    public void setSecondPlace(Team secondPlace) {
        this.secondPlace = secondPlace;
    }

    public Team getThirdPlace() {
        return thirdPlace;
    }

    public void setThirdPlace(Team thirdPlace) {
        this.thirdPlace = thirdPlace;
    }

    @JsonIgnore
    public List<Team> getPodium() {
        List<Team> list = new ArrayList<>();
        list.add(firstPlace);
        list.add(secondPlace);
        list.add(thirdPlace);
        return list;
    }

    public static class Builder {
        private List<MatchResult> matchResults = new ArrayList<>();
        private Player goldenBoot;
        private Team firstPlace;
        private Team secondPlace;
        private Team thirdPlace;

        public Builder withMatchResults(List<MatchResult> matchResults) {
            this.matchResults = matchResults;
            return this;
        }

        public Builder withGolderBoot(Player goldenBoot) {
            this.goldenBoot = goldenBoot;
            return this;
        }

        public Builder withFirstPlace(Team firstPlace) {
            this.firstPlace = firstPlace;
            return this;
        }

        public Builder withSecondPlace(Team secondPlace) {
            this.secondPlace = secondPlace;
            return this;
        }

        public Builder withThirdPlace(Team thirdPlace) {
            this.thirdPlace = thirdPlace;
            return this;
        }

        public Bet build() {
            return new Bet(this);
        }
    }
}
