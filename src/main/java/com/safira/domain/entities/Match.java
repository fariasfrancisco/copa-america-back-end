package com.safira.domain.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.safira.domain.LocalDateTimeDeserializer;
import com.safira.domain.LocalDateTimeSerializer;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.safira.common.Regex.GROUP_STAGE_REGEX;

@Entity
@Table(name = "[match]")
public class Match extends AbstractPersistable<Long> {

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    private LocalDateTime date;

    @Column(nullable = false, unique = true)
    private String identifier;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "homeTeam_id", nullable = true)
    private Team homeTeam;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "awayTeam_id", nullable = true)
    private Team awayTeam;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "match")
    private List<Goal> goals = new ArrayList<>();

    @Column(nullable = false)
    private Integer homePenalties;

    @Column(nullable = false)
    private Integer awayPenalties;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "stage_id", nullable = false)
    private Stage stage;

    public Match() {
    }

    public Match(LocalDateTime date,
                 Team homeTeam,
                 Team awayTeam,
                 List<Goal> goals,
                 Stage stage,
                 String identifier,
                 Integer homePenalties,
                 Integer awayPenalties) {
        this.date = date;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.goals = goals;
        this.stage = stage;
        this.identifier = identifier;
        this.homePenalties = homePenalties;
        this.awayPenalties = awayPenalties;
    }

    public Match(Builder builder) {
        this.date = builder.date;
        this.homeTeam = builder.homeTeam;
        this.awayTeam = builder.awayTeam;
        this.goals = builder.goals;
        for (Goal goal : goals) if (goal.getMatch() != this) goal.setMatch(this);
        this.stage = builder.stage;
        this.identifier = builder.identifier;
        this.homePenalties = 0;
        this.awayPenalties = 0;
    }

    @JsonIgnore
    public Long getId() {
        return super.getId();
    }

    @JsonIgnore
    public boolean isNew() {
        return super.isNew();
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Integer getHomePenalties() {
        return homePenalties;
    }

    public void setHomePenalties(Integer homePenalties) {
        this.homePenalties = homePenalties;
    }

    public Integer getAwayPenalties() {
        return awayPenalties;
    }

    public void setAwayPenalties(Integer awayPenalties) {
        this.awayPenalties = awayPenalties;
    }

    public static class Builder {

        private LocalDateTime date;
        private Team homeTeam;
        private Team awayTeam;
        private List<Goal> goals = new ArrayList<>();
        private Stage stage;
        private String identifier;

        public Builder withDate(LocalDateTime date) {
            this.date = date;
            return this;
        }

        public Builder withHomeTeam(Team homeTeam) {
            this.homeTeam = homeTeam;
            return this;
        }

        public Builder withAwayTeam(Team awayTeam) {
            this.awayTeam = awayTeam;
            return this;
        }

        public Builder withGoals(List<Goal> goals) {
            this.goals = goals;
            return this;
        }

        public Builder withStage(Stage stage) {
            this.stage = stage;
            return this;
        }

        public Builder withidentifier(String identifier) {
            this.identifier = identifier;
            return this;
        }

        public Match build() {
            return new Match(this);
        }

    }

    @JsonIgnore
    public boolean isPlayed() {
        LocalDateTime now = LocalDateTime.now();
        return date.plusMinutes(150).isBefore(now);
    }

    public Optional<Team> calculateWinner() {
        Integer homeGoals = getHomeGoals();
        Integer awayGoals = getAwayGoals();
        if (homeGoals > awayGoals) return Optional.of(homeTeam);
        else if (awayGoals > homeGoals) return Optional.of(awayTeam);
        else if (identifier.matches(GROUP_STAGE_REGEX)) return Optional.empty();
        else if (homePenalties > awayPenalties) return Optional.of(homeTeam);
        else if (awayPenalties > homePenalties) return Optional.of(awayTeam);
        else return Optional.empty();
    }

    public Team calculateLoser() {
        if (calculateWinner().get().equals(homeTeam)) return awayTeam;
        else return homeTeam;
    }

    @JsonIgnore
    public Integer getHomeGoals() {
        Integer homeGoals = 0;
        for (Goal goal : goals) {
            Player player = goal.getPlayer();
            Team team = player.getTeam();
            if (team.equals(homeTeam)) homeGoals++;
        }
        return homeGoals;
    }

    @JsonIgnore
    public Integer getAwayGoals() {
        Integer awayGoals = 0;
        for (Goal goal : goals) {
            Player player = goal.getPlayer();
            Team team = player.getTeam();
            if (team.equals(awayTeam)) awayGoals++;
        }
        return awayGoals;
    }
}
