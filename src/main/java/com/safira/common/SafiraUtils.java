package com.safira.common;

import java.util.*;

public class SafiraUtils {
    public static <E> Collection<E> toCollection(Iterable<E> iterable) {
        Collection<E> list = new ArrayList<>();
        for (E item : iterable) list.add(item);
        return list;
    }

    public static <E> List<E> toList(Iterable<E> iterable) {
        ArrayList<E> list = new ArrayList<>();
        if (iterable instanceof List) return (List<E>) iterable;
        if (iterable != null) for (E e : iterable) list.add(e);
        return list;
    }

    public static <E> List<E> toList(E[] array) {
        List<E> list = new ArrayList<>();
        Collections.addAll(list, array);
        return list;
    }

    public static <T> Set<T> toSet(Iterable<T> iterable) {
        Set<T> set = new HashSet<>();
        for (T t : iterable) set.add(t);
        return set;
    }

    public static <T> T[] toArray(Iterable<T> iterable) {
        return (T[]) toList(iterable).toArray();
    }

    public static <T> List<T> union(Iterable<T> iterable1, Iterable<T> iterable2) {
        List<T> list = new ArrayList<>();
        for (T t : iterable1) list.add(t);
        for (T t : iterable2) if (!list.contains(t)) list.add(t);
        return list;
    }

    public static <T> List<T> intersection(Iterable<T> iterable1, Iterable<T> iterable2) {
        List<T> aux = new ArrayList<>();
        List<T> out = new ArrayList<>();
        for (T t : iterable1) aux.add(t);
        for (T t : iterable2) if (aux.contains(t)) out.add(t);
        return out;
    }

    public static <T> boolean contains(T[] array, T value) {
        List<T> list = toList(array);
        return list.contains(value);
    }

    public static <T> boolean isNull(T object) {
        return object == null;
    }
}
