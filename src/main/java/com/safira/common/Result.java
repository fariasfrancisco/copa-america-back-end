package com.safira.common;

public class Result {
    private String identifier;
    private Integer homeGoals;
    private Integer awayGoals;
    private Integer homePenalties;
    private Integer awayPenalties;

    public Result() {
    }

    public Result(String identifier,
                  Integer homeGoals,
                  Integer awayGoals,
                  Integer homePenalties,
                  Integer awayPenalties) {
        this.identifier = identifier;
        this.homeGoals = homeGoals;
        this.awayGoals = awayGoals;
        this.homePenalties = homePenalties;
        this.awayPenalties = awayPenalties;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Integer getHomeGoals() {
        return homeGoals;
    }

    public void setHomeGoals(Integer homeGoals) {
        this.homeGoals = homeGoals;
    }

    public Integer getAwayGoals() {
        return awayGoals;
    }

    public void setAwayGoals(Integer awayGoals) {
        this.awayGoals = awayGoals;
    }

    public Integer getHomePenalties() {
        return homePenalties;
    }

    public void setHomePenalties(Integer homePenalties) {
        this.homePenalties = homePenalties;
    }

    public Integer getAwayPenalties() {
        return awayPenalties;
    }

    public void setAwayPenalties(Integer awayPenalties) {
        this.awayPenalties = awayPenalties;
    }
}
