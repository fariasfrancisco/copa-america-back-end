package com.safira.common;

/**
 * Created by developer on 05/06/15.
 */
public class StageName {
    private String stageName;

    public StageName() {
    }

    public StageName(String stageName) {
        this.stageName = stageName;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }
}
