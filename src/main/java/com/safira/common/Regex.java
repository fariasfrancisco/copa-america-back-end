package com.safira.common;

/**
 * Created by developer on 09/06/15.
 */
public class Regex {

    public static final String GROUP_STAGE_REGEX = "^g[a-c][1-6]$";
    public static final String GROUP_A_REGEX = "^ga[1-6]$";
    public static final String GROUP_B_REGEX = "^gb[1-6]$";
    public static final String GROUP_C_REGEX = "^gc[1-6]$";

    public static final String QUARTER_FINALS_REGEX = "^q[1-4]$";
    public static final String SEMI_FINALS_REGEX = "^s[1-2]$";
    public static final String FINALS_REGEX = "^f[1-2]$";
}
