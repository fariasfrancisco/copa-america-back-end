package com.safira.common.exceptions;

public class EmptyQueryResultException extends SafiraException {
    public EmptyQueryResultException() {
        super("The requested query returned an empty result set.");
    }
}

