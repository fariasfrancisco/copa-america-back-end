package com.safira.common.exceptions;

public class LoginException extends SafiraException {
    public LoginException() {
        super("Failure to login with given information.");
    }
}
