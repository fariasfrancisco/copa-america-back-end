package com.safira.common.exceptions;

public class InconsistencyException extends SafiraException {
    public InconsistencyException() {
        super("Inconsistency found between the recieved fields.");
    }
}
