package com.safira.common.exceptions;

public class ValidatorException extends SafiraException {
    public ValidatorException() {
        super("Validation Exception found.");
    }
}
