package com.safira.common;

public class MatchDate {
    private int day;
    private int month;
    private int minutes;
    private int hour;

    public MatchDate() {
    }

    public MatchDate(int day, int month, int minutes, int hour) {
        this.day = day;
        this.month = month;
        this.minutes = minutes;
        this.hour = hour;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }
}
