package com.safira.common;

/**
 * Created by developer on 05/06/15.
 */
public class GoalScorer {
    private String name;

    public GoalScorer() {
    }

    public GoalScorer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
