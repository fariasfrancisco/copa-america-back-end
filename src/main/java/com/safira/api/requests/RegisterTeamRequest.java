package com.safira.api.requests;

public class RegisterTeamRequest {
    private String name;

    public RegisterTeamRequest() {
    }

    public RegisterTeamRequest(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
