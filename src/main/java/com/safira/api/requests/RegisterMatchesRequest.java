package com.safira.api.requests;

import java.util.List;

public class RegisterMatchesRequest {
    private List<RegisterMatchRequest> registerMatchRequests;

    public RegisterMatchesRequest() {
    }

    public RegisterMatchesRequest(List<RegisterMatchRequest> registerMatchRequests) {
        this.registerMatchRequests = registerMatchRequests;
    }

    public List<RegisterMatchRequest> getRegisterMatchRequests() {
        return registerMatchRequests;
    }

    public void setRegisterMatchRequests(List<RegisterMatchRequest> registerMatchRequests) {
        this.registerMatchRequests = registerMatchRequests;
    }
}
