package com.safira.api.requests;

public class RegisterPlayerRequest {
    private String fullName;
    private String teamName;

    public RegisterPlayerRequest() {
    }

    public RegisterPlayerRequest(String fullName, String teamName) {
        this.fullName = fullName;
        this.teamName = teamName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }
}
