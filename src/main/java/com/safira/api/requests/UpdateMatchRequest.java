package com.safira.api.requests;

import com.safira.common.GoalScorer;

import java.util.List;

public class UpdateMatchRequest {
    private String identifier;
    private List<GoalScorer> goalScorersHome;
    private List<GoalScorer> goalScorersAway;
    private Integer homePenalties;
    private Integer awayPenalties;

    public UpdateMatchRequest() {
    }

    public UpdateMatchRequest(String identifier,
                              List<GoalScorer> goalScorersHome,
                              List<GoalScorer> goalScorersAway,
                              Integer homePenalties,
                              Integer awayPenalties) {
        this.identifier = identifier;
        this.goalScorersHome = goalScorersHome;
        this.goalScorersAway = goalScorersAway;
        this.homePenalties = homePenalties;
        this.awayPenalties = awayPenalties;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public List<GoalScorer> getGoalScorersHome() {
        return goalScorersHome;
    }

    public void setGoalScorersHome(List<GoalScorer> goalScorersHome) {
        this.goalScorersHome = goalScorersHome;
    }

    public List<GoalScorer> getGoalScorersAway() {
        return goalScorersAway;
    }

    public void setGoalScorersAway(List<GoalScorer> goalScorersAway) {
        this.goalScorersAway = goalScorersAway;
    }

    public Integer getHomePenalties() {
        return homePenalties;
    }

    public void setHomePenalties(Integer homePenalties) {
        this.homePenalties = homePenalties;
    }

    public Integer getAwayPenalties() {
        return awayPenalties;
    }

    public void setAwayPenalties(Integer awayPenalties) {
        this.awayPenalties = awayPenalties;
    }
}
