package com.safira.api.requests;

import java.util.List;

public class RegisterTeamsRequest {
    private List<RegisterTeamRequest> registerTeamRequests;

    public RegisterTeamsRequest() {
    }

    public RegisterTeamsRequest(List<RegisterTeamRequest> registerTeamRequests) {
        this.registerTeamRequests = registerTeamRequests;
    }

    public List<RegisterTeamRequest> getRegisterTeamRequests() {
        return registerTeamRequests;
    }

    public void setRegisterTeamRequests(List<RegisterTeamRequest> registerPlayerRequests) {
        this.registerTeamRequests = registerPlayerRequests;
    }

}
