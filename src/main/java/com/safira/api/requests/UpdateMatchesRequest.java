package com.safira.api.requests;

import java.util.List;

/**
 * Created by francisco on 08/06/15.
 */
public class UpdateMatchesRequest {
    private List<UpdateMatchRequest> updateMatchRequests;

    public UpdateMatchesRequest() {
    }

    public UpdateMatchesRequest(List<UpdateMatchRequest> updateMatchRequests) {
        this.updateMatchRequests = updateMatchRequests;
    }

    public List<UpdateMatchRequest> getUpdateMatchRequests() {
        return updateMatchRequests;
    }

    public void setUpdateMatchRequests(List<UpdateMatchRequest> updateMatchRequests) {
        this.updateMatchRequests = updateMatchRequests;
    }
}
