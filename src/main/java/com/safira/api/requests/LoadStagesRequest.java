package com.safira.api.requests;

import com.safira.common.StageName;

import java.util.List;

/**
 * Created by developer on 05/06/15.
 */
public class LoadStagesRequest {
    private List<StageName> stageNames;

    public LoadStagesRequest() {
    }

    public LoadStagesRequest(List<StageName> stageNames) {
        this.stageNames = stageNames;
    }

    public List<StageName> getStageNames() {
        return stageNames;
    }

    public void setStageNames(List<StageName> stageNames) {
        this.stageNames = stageNames;
    }
}
