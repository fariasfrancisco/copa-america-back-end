package com.safira.api.requests;

import com.safira.domain.TeamStats;

import java.util.List;

/**
 * Created by developer on 10/06/15.
 */
public class ThirdPlaceTeams {
    private List<TeamStats> teamStatsList;

    public ThirdPlaceTeams() {
    }

    public ThirdPlaceTeams(List<TeamStats> teamStatsList) {
        this.teamStatsList = teamStatsList;
    }

    public List<TeamStats> getTeamStatsList() {
        return teamStatsList;
    }

    public void setTeamStatsList(List<TeamStats> teamStatsList) {
        this.teamStatsList = teamStatsList;
    }
}
