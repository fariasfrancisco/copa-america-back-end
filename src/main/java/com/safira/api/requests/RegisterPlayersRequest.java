package com.safira.api.requests;

import java.util.List;

public class RegisterPlayersRequest {
    private List<RegisterPlayerRequest> registerPlayerRequests;

    public RegisterPlayersRequest() {
    }

    public RegisterPlayersRequest(List<RegisterPlayerRequest> registerPlayerRequests) {
        this.registerPlayerRequests = registerPlayerRequests;
    }

    public List<RegisterPlayerRequest> getRegisterPlayerRequests() {
        return registerPlayerRequests;
    }

    public void setRegisterPlayerRequests(List<RegisterPlayerRequest> registerPlayerRequests) {
        this.registerPlayerRequests = registerPlayerRequests;
    }
}
