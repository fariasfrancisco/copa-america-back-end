package com.safira.api.requests;

import com.safira.common.MatchDate;

import java.time.LocalDateTime;

public class RegisterMatchRequest {
    private MatchDate date;
    private String homeTeamName;
    private String awayTeamName;
    private String identifier;
    private String stageName;

    public RegisterMatchRequest() {
    }

    public RegisterMatchRequest(MatchDate date,
                                String homeTeamName,
                                String awayTeamName,
                                String identifier,
                                String stage) {
        this.date = date;
        this.homeTeamName = homeTeamName;
        this.awayTeamName = awayTeamName;
        this.identifier = identifier;
        this.stageName = stage;
    }

    public MatchDate getDate() {
        return date;
    }

    public void setDate(MatchDate date) {
        this.date = date;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getStageName() {
        return stageName;
    }

    public void setStageName(String stageName) {
        this.stageName = stageName;
    }

    public LocalDateTime getDateAsLocalDateTime() {
        return LocalDateTime.of(2015, date.getMonth(), date.getDay(), date.getHour(), date.getMinutes());
    }
}
