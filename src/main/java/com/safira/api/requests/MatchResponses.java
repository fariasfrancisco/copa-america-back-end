package com.safira.api.requests;

import com.safira.api.responses.MatchResponse;

import java.util.List;

public class MatchResponses {
    private List<MatchResponse> matchResponses;

    public MatchResponses(List<MatchResponse> matchResponses) {
        this.matchResponses = matchResponses;
    }

    public MatchResponses() {
    }

    public List<MatchResponse> getMatchResponses() {
        return matchResponses;
    }

    public void setMatchResponses(List<MatchResponse> matchResponses) {
        this.matchResponses = matchResponses;
    }
}
