package com.safira.api.requests;

import com.safira.common.Result;

import java.util.List;

public class RegisterBetRequest {
    private String username;
    private List<Result> results;
    private String goldenBootTeam;
    private String goldenBootPlayer;
    private String firstPlaceTeam;
    private String secondPlaceTeam;
    private String thirdPlaceTeam;

    public RegisterBetRequest() {
    }

    public RegisterBetRequest(String username,
                              List<Result> results,
                              String goldenBootTeam,
                              String goldenBootPlayer,
                              String firstPlaceTeam,
                              String secondPlaceTeam,
                              String thirdPlaceTeam) {
        this.username = username;
        this.results = results;
        this.goldenBootTeam = goldenBootTeam;
        this.goldenBootPlayer = goldenBootPlayer;
        this.firstPlaceTeam = firstPlaceTeam;
        this.secondPlaceTeam = secondPlaceTeam;
        this.thirdPlaceTeam = thirdPlaceTeam;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public String getGoldenBootTeam() {
        return goldenBootTeam;
    }

    public void setGoldenBootTeam(String goldenBootTeam) {
        this.goldenBootTeam = goldenBootTeam;
    }

    public String getGoldenBootPlayer() {
        return goldenBootPlayer;
    }

    public void setGoldenBootPlayer(String goldenBootPlayer) {
        this.goldenBootPlayer = goldenBootPlayer;
    }

    public String getFirstPlaceTeam() {
        return firstPlaceTeam;
    }

    public void setFirstPlaceTeam(String firstPlaceTeam) {
        this.firstPlaceTeam = firstPlaceTeam;
    }

    public String getSecondPlaceTeam() {
        return secondPlaceTeam;
    }

    public void setSecondPlaceTeam(String secondPlaceTeam) {
        this.secondPlaceTeam = secondPlaceTeam;
    }

    public String getThirdPlaceTeam() {
        return thirdPlaceTeam;
    }

    public void setThirdPlaceTeam(String thirdPlaceTeam) {
        this.thirdPlaceTeam = thirdPlaceTeam;
    }
}
