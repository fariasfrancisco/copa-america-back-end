package com.safira.api.controllers;

import com.safira.api.requests.RegisterPlayerRequest;
import com.safira.api.requests.RegisterPlayersRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.entities.Player;
import com.safira.service.interfaces.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PlayerController {

    @Autowired
    private ErrorOutput errors;

    @Autowired
    private PlayerService playerService;

    /**
     * Creates a player with the received parameters.
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, CREATED);
     * @path '/player/post';
     * @method POST;
     */
    @RequestMapping(value = "/player/post", method = RequestMethod.POST)
    public ResponseEntity postPlayer(@RequestBody RegisterPlayerRequest request) {
        errors.flush();
        Player player;
        try {
            player = playerService.registerPlayer(request, errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(player, HttpStatus.CREATED);
    }

    /**
     * Creates players with the received parameters.
     *
     * @param requests;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, CREATED);
     * @path '/player/postBatch';
     * @method POST;
     */
    @RequestMapping(value = "/player/postBatch", method = RequestMethod.POST)
    public ResponseEntity postPlayers(@RequestBody RegisterPlayersRequest requests) {
        errors.flush();
        List<Player> players;
        try {
            players = playerService.registerPlayers(requests, errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(players, HttpStatus.CREATED);
    }

    /**
     * Returns the players of the team with the name received in the path.
     *
     * @param teamName;
     * @return HTTPResponse(NOT_FOUND, INTERNAL_SERVER_ERROR, OK);
     * @path '/player/getByTeam/{teamName}';
     * @method GET;
     */
    @RequestMapping(value = "/player/getByTeam/{teamName}", method = RequestMethod.GET)
    public ResponseEntity getPlayersByTeam(@PathVariable(value = "teamName") String teamName) {
        errors.flush();
        List<Player> players;
        try {
            players = playerService.getPlayersByTeam(teamName, errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(players, HttpStatus.OK);
    }

    /**
     * Returns the player with the name received in the parameter.
     *
     * @param name;
     * @return HTTPResponse(NOT_FOUND, INTERNAL_SERVER_ERROR, OK);
     * @path '/player/getByName';
     * @method GET;
     */
    @RequestMapping(value = "/player/getByName", method = RequestMethod.GET)
    public ResponseEntity getPlayersByName(@RequestParam(value = "name") String name) {
        errors.flush();
        List<Player> players;
        try {
            players = playerService.getPlayersByName(name, errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(players, HttpStatus.OK);
    }
}
