package com.safira.api.controllers;

import com.safira.api.requests.RegisterTeamRequest;
import com.safira.api.requests.RegisterTeamsRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.entities.Team;
import com.safira.service.interfaces.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TeamController {

    @Autowired
    private TeamService teamService;

    @Autowired
    private ErrorOutput errors;

    /**
     * Creates the team received as parameter;
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, CREATED);
     * @path '/team/post';
     * @method POST;
     */
    @RequestMapping(value = "/team/post", method = RequestMethod.POST)
    public ResponseEntity postTeam(@RequestBody RegisterTeamRequest request) {
        errors.flush();
        Team team;
        try {
            team = teamService.registerTeam(request, errors);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(team, HttpStatus.CREATED);
    }

    /**
     * Creates the teams received as parameters;
     *
     * @param requests;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, CREATED);
     * @path '/team/postBatch';
     * @method POST;
     */
    @RequestMapping(value = "/team/postBatch", method = RequestMethod.POST)
    public ResponseEntity postTeams(@RequestBody RegisterTeamsRequest requests) {
        errors.flush();
        List<Team> teams;
        try {
            teams = teamService.registerTeams(requests, errors);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(teams, HttpStatus.CREATED);
    }

    /**
     * Returns a team by name.
     *
     * @param name;
     * @return HTTPResponse(INTERNAL_SERVER_ERROR, NOT_FOUND, OK);
     * @path '/team/get';
     * @method GET;
     */
    @RequestMapping(value = "/team/get", method = RequestMethod.GET)
    public ResponseEntity get(@RequestParam(value = "name", required = true) String name) {
        errors.flush();
        Team team;
        try {
            team = teamService.getTeamByName(name, errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(team, HttpStatus.OK);
    }

    /**
     * Returns all teams.
     *
     * @return HTTPResponse(INTERNAL_SERVER_ERROR, OK);
     * @path '/team/getAll';
     * @method GET;
     */
    @RequestMapping(value = "/team/getAll", method = RequestMethod.GET)
    public ResponseEntity getAll() {
        errors.flush();
        List<Team> teams;
        try {
            teams = teamService.getAllTeams(errors);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(teams, HttpStatus.OK);
    }
}
