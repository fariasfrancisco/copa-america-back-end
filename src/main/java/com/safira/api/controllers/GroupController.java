package com.safira.api.controllers;

import com.safira.api.requests.MatchResponses;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.Group;
import com.safira.service.interfaces.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GroupController {
    @Autowired
    private GroupService groupService;

    @Autowired
    private ErrorOutput errors;

    /**
     * Builds and returns a group with the given parameters.
     *
     * @param responses;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, OK)
     * @path '/group';
     * @method POST;
     */
    @RequestMapping(value = "/group", method = RequestMethod.POST)
    public ResponseEntity post(@RequestBody MatchResponses responses) {
        errors.flush();
        Group group;
        try {
            group = groupService.buildGroup(responses, errors);
            return new ResponseEntity<>(group, HttpStatus.OK);
        } catch (EmptyQueryResultException e) {
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
