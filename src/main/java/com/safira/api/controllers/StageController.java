package com.safira.api.controllers;

import com.safira.api.requests.LoadStagesRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.domain.entities.Stage;
import com.safira.service.interfaces.StageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StageController {
    @Autowired
    private StageService stageService;

    @Autowired
    private ErrorOutput errors;

    /**
     * Initializes the stages in the server. MUST ONLY BE USED ONCE!
     *
     * @param request;
     * @return HTTPResponse(INTERNAL_SERVER_ERROR, OK);
     * @path '/stage/initialize';
     * @method POST;
     */
    @RequestMapping(value = "/stage/initialize", method = RequestMethod.POST)
    public ResponseEntity init(@RequestBody LoadStagesRequest request) {
        errors.flush();
        List<Stage> stages;
        try {
            stages = stageService.loadStages(request, errors);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(stages, HttpStatus.OK);
    }
}
