package com.safira.api.controllers;

import com.safira.api.requests.RegisterMatchRequest;
import com.safira.api.requests.RegisterMatchesRequest;
import com.safira.api.requests.UpdateMatchRequest;
import com.safira.api.requests.UpdateMatchesRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.api.responses.MatchResponse;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.common.exceptions.ValidatorException;
import com.safira.domain.entities.Match;
import com.safira.service.Validator;
import com.safira.service.interfaces.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MatchController {

    @Autowired
    private ErrorOutput errors;

    @Autowired
    private MatchService matchService;

    /**
     * Registers the match passed as parameter.
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, CREATED)
     * @path '/match/post';
     * @method POST;
     */
    @RequestMapping(value = "/match/post", method = RequestMethod.POST)
    public ResponseEntity post(@RequestBody RegisterMatchRequest request) {
        errors.flush();
        MatchResponse matchResponse;
        try {
            Validator.validateMatchRequest(request, errors);
            matchResponse = matchService.registerMatch(request, errors);
        } catch (ValidatorException | EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(matchResponse, HttpStatus.CREATED);
    }

    /**
     * Registers the matches passed as parameter.
     *
     * @param requests;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, CREATED)
     * @path '/match/postBatch';
     * @method POST;
     */
    @RequestMapping(value = "/match/postBatch", method = RequestMethod.POST)
    public ResponseEntity postPlayers(@RequestBody RegisterMatchesRequest requests) {
        errors.flush();
        List<MatchResponse> matchResponses;
        try {
            Validator.validateMatchRequests(requests, errors);
            matchResponses = matchService.registerMatches(requests, errors);
        } catch (ValidatorException | EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(matchResponses, HttpStatus.CREATED);
    }

    /**
     * Updates a match with given parameters.
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, CREATED)
     * @path '/match/update';
     * @method POST;
     */
    @RequestMapping(value = "/match/update", method = RequestMethod.POST)
    public ResponseEntity update(@RequestBody UpdateMatchRequest request) {
        errors.flush();
        MatchResponse matchResponse;
        try {
            Validator.validateUpdateMatchRequest(request, errors);
            matchResponse = matchService.updateMatch(request, errors);
        } catch (ValidatorException | EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(matchResponse, HttpStatus.CREATED);
    }

    /**
     * Updates matches with given parameters.
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, OK)
     * @path '/match/updateBatch';
     * @method POST;
     */
    @RequestMapping(value = "/match/updateBatch", method = RequestMethod.POST)
    public ResponseEntity postPlayers(@RequestBody UpdateMatchesRequest request) {
        errors.flush();
        List<MatchResponse> matchResponses;
        try {
            Validator.validateUpdateMatchesRequest(request, errors);
            matchResponses = matchService.updateMatches(request, errors);
        } catch (ValidatorException | EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(matchResponses, HttpStatus.OK);
    }

    /**
     * Returns all matches of a given stage.
     *
     * @param stageName;
     * @return HTTPResponse(NOT_FOUND, INTERNAL_SERVER_ERROR, OK);
     * @path '/match/getByStage/{stageName}';
     * @method GET;
     */
    @RequestMapping(value = "/match/getByStage/{stageName}", method = RequestMethod.GET)
    public ResponseEntity getByStage(@PathVariable(value = "stageName") String stageName) {
        errors.flush();
        List<MatchResponse> matchResponses;
        try {
            matchResponses = matchService.getMatchByStage(stageName, errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(matchResponses, HttpStatus.OK);
    }

    /**
     * Returns the match that corresponds to the passed identifier.
     *
     * @param identifier;
     * @return HTTPResponse(NOT_FOUND, INTERNAL_SERVER_ERROR, OK);
     * @path '/match/getByIdentifier/{identifier}';
     * @method GET;
     */
    @RequestMapping(value = "/match/getByIdentifier/{identifier}", method = RequestMethod.GET)
    public ResponseEntity getByIdentifier(@PathVariable(value = "identifier") String identifier) {
        errors.flush();
        Match match;
        try {
            match = matchService.getMatchByIdentifier(identifier, errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.NOT_FOUND);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(match, HttpStatus.OK);
    }
}

