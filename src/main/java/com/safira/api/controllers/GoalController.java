package com.safira.api.controllers;

import com.safira.api.responses.Scorer;
import com.safira.service.interfaces.GoalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class GoalController {
    @Autowired
    private GoalService goalService;

    /**
     * Returns the current contestants for the Golden Boot.
     *
     * @return HTTPResponse(INTERNAL_SERVER_ERROR, OK);
     * @path '/goal/getGoldenBoot';
     * @method GET;
     */
    @RequestMapping(value = "/goal/getGoldenBoot", method = RequestMethod.GET)
    public ResponseEntity goldenBoot() {
        List<Scorer> topScorers;
        try {
            topScorers = goalService.getGoldenBootAsScorerList();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(topScorers, HttpStatus.OK);
    }

    /**
     * Returns the current top scorers.
     *
     * @return HTTPResponse(INTERNAL_SERVER_ERROR, OK);
     * @path '/goal/getTopScorers';
     * @method GET;
     */
    @RequestMapping(value = "/goal/getTopScorers", method = RequestMethod.GET)
    public ResponseEntity topScorers() {
        List<Scorer> topScorers;
        try {
            topScorers = goalService.getTop5Scorers();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(topScorers, HttpStatus.OK);
    }
}
