package com.safira.api.controllers;

import com.safira.api.requests.LoginBetRequest;
import com.safira.api.requests.RegisterAccountRequest;
import com.safira.api.requests.RegisterBetRequest;
import com.safira.api.responses.BetPoints;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.common.exceptions.LoginException;
import com.safira.common.exceptions.ValidatorException;
import com.safira.domain.entities.Bet;
import com.safira.domain.entities.BetAccount;
import com.safira.service.interfaces.BetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BetController {
    @Autowired
    private ErrorOutput errors;

    @Autowired
    private BetService betService;

    /**
     * Register the user's account.
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, CREATED);
     * @path '/bet/register';
     * @method POST;
     */
    @RequestMapping(value = "/bet/register", method = RequestMethod.POST)
    public ResponseEntity postBetAccount(@RequestBody RegisterAccountRequest request) {
        errors.flush();
        BetAccount betAccount;
        try {
            betAccount = betService.registerAccount(request, errors);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(betAccount, HttpStatus.CREATED);
    }

    /**
     * Registers the user's bet.
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, CREATED);
     * @path '/bet/post';
     * @method POST;
     */
    @RequestMapping(value = "/bet/post", method = RequestMethod.POST)
    public ResponseEntity postBet(@RequestBody RegisterBetRequest request) {
        errors.flush();
        Bet bet;
        try {
            bet = betService.registerBet(request, errors);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        return new ResponseEntity<>(bet, HttpStatus.CREATED);
    }

    /**
     * Logs the user in.
     *
     * @param request;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, OK);
     * @path '/bet/login';
     * @method POST;
     */
    @RequestMapping(value = "/bet/login", method = RequestMethod.POST)
    public ResponseEntity getBet(@RequestBody LoginBetRequest request) {
        errors.flush();
        BetAccount bet;
        try {
            bet = betService.retrieveBetByUsername(request, errors);
        } catch (LoginException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(bet, HttpStatus.OK);
    }

    /**
     * Validates the user's account.
     *
     * @param username;
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, OK);
     * @path '/bet/validate';
     * @method GET;
     */
    @RequestMapping(value = "/bet/validate", method = RequestMethod.GET)
    public ResponseEntity validate(@RequestParam(value = "user") String username) {
        errors.flush();
        BetAccount betAccount;
        try {
            betAccount = betService.validateBet(username, errors);
        } catch (ValidatorException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(betAccount, HttpStatus.OK);
    }

    /**
     * Returns all bets' points.
     *
     * @return HTTPResponse(UNPROCESSABLE_ENTITY, INTERNAL_SERVER_ERROR, OK);
     * @path '/bet/getAllPoints';
     * @method GET;
     */
    @RequestMapping(value = "/bet/getAllPoints", method = RequestMethod.GET)
    public ResponseEntity getBetPoints() {
        errors.flush();
        List<BetPoints> betPoints;
        try {
            betPoints = betService.retrieveAllBetPoints(errors);
        } catch (EmptyQueryResultException e) {
            e.printStackTrace();
            return new ResponseEntity<>(errors, HttpStatus.UNPROCESSABLE_ENTITY);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(betPoints, HttpStatus.OK);
    }
}
