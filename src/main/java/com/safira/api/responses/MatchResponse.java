package com.safira.api.responses;

import com.safira.common.SafiraUtils;
import com.safira.domain.entities.Match;
import com.safira.domain.entities.Team;

/**
 * Created by developer on 09/06/15.
 */
public class MatchResponse {
    private String identifier;
    private String homeTeam;
    private String awayTeam;
    private Integer homeGoals;
    private Integer awayGoals;
    private Integer homePenalties;
    private Integer awayPenalties;

    public MatchResponse() {
    }

    public MatchResponse(String identifier,
                         String homeTeam,
                         String awayTeam,
                         Integer homeGoals,
                         Integer awayGoals,
                         Integer homePenalties,
                         Integer awayPenalties) {
        this.identifier = identifier;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.homeGoals = homeGoals;
        this.awayGoals = awayGoals;
        this.homePenalties = homePenalties;
        this.awayPenalties = awayPenalties;
    }

    public MatchResponse(Match match) {
        this.identifier = match.getIdentifier();
        Team homeTeam = match.getHomeTeam();
        this.homeTeam = SafiraUtils.isNull(homeTeam) ? null : match.getHomeTeam().getName();
        Team awayTeam = match.getAwayTeam();
        this.awayTeam = SafiraUtils.isNull(awayTeam) ? null : match.getAwayTeam().getName();
        this.homeGoals = match.getHomeGoals();
        this.awayGoals = match.getAwayGoals();
        this.homePenalties = match.getHomePenalties();
        this.awayPenalties = match.getAwayPenalties();
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Integer getHomeGoals() {
        return homeGoals;
    }

    public void setHomeGoals(Integer homeGoals) {
        this.homeGoals = homeGoals;
    }

    public Integer getAwayGoals() {
        return awayGoals;
    }

    public void setAwayGoals(Integer awayGoals) {
        this.awayGoals = awayGoals;
    }

    public Integer getHomePenalties() {
        return homePenalties;
    }

    public void setHomePenalties(Integer homePenalties) {
        this.homePenalties = homePenalties;
    }

    public Integer getAwayPenalties() {
        return awayPenalties;
    }

    public void setAwayPenalties(Integer awayPenalties) {
        this.awayPenalties = awayPenalties;
    }

    public Integer calculateWinner() {
        int goalDiff = homeGoals - awayGoals;
        int penaltyDiff = homeGoals - awayGoals;

        if (goalDiff > 0) return 1;
        else if (goalDiff < 0) return -1;
        else if (penaltyDiff > 0) return 1;
        else if (penaltyDiff < 0) return -1;
        else return 0;
    }
}

