package com.safira.api.responses;

import com.safira.domain.entities.*;
import com.safira.service.interfaces.GoalService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.safira.common.Regex.*;

/**
 * The points are awarded in the following fashion:
 * <h1>Matches:</h1> For each match, guessing the correct outcome yields 1 point.
 * Guessing the exact result adds 2 more points.
 * <p>
 * <h1>Group Stage:</h1> If all the matches in a group are guessed correctly 12 points are awarded.
 * If this bonus is achieved in all three groups, then another 10 points are awarded.
 * <p>
 * <h1>Quarter Finals:</h1> If all the matches in this stage are guessed correctly, 8 more points will be awarded.
 * <p>
 * <h1>Semi Finals:</h1> If all the matches in this stage are guessed correctly, 4 more points will be awarded.
 * <p>
 * <h1>Finals:</h1> If all the matches in this stage are guessed correctly, 4 more points will be awarded.
 * <p>
 * <h1>Podium:</h1> If the three teams are guesses correctly, 30 points will be awarded.
 * If the order is correctly guesses as well 30 more points will be given.
 * <p>
 * <h1>Golden Boot:</h1> If the golden boot is guessed correctly, 50 points will be awarded.
 * <p>
 * The maximum amount of points to be earned is 250.
 */
public class BetPoints {

    @Autowired
    private GoalService goalService;

    private String username;
    private Boolean validated;
    private Integer groupAPoints;
    private Integer groupBPoints;
    private Integer groupCPoints;
    private Integer perfectGroupPhase;
    private Integer quarterFinalsPoints;
    private Integer semiFinalsPoints;
    private Integer finalsPoints;
    private Integer podiumPoints;
    private Integer goldenBootPoints;

    public BetPoints(BetAccount betAccount) {
        Bet bet = betAccount.getBet();
        this.username = betAccount.getUsername();
        this.validated = betAccount.isValidated();
        this.groupAPoints = calculateGroup(bet, GROUP_A_REGEX);
        this.groupBPoints = calculateGroup(bet, GROUP_B_REGEX);
        this.groupCPoints = calculateGroup(bet, GROUP_C_REGEX);
        this.perfectGroupPhase = 0;
        if ((groupAPoints + groupBPoints + groupCPoints) == 90)
            this.perfectGroupPhase += 10;
        this.quarterFinalsPoints = calculateKnockOut(bet, QUARTER_FINALS_REGEX);
        this.semiFinalsPoints = calculateKnockOut(bet, SEMI_FINALS_REGEX);
        this.finalsPoints = calculateKnockOut(bet, FINALS_REGEX);
        this.podiumPoints = calculatePodium(bet);
        this.goldenBootPoints = calculateGoldenBoot(bet);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Integer getGroupAPoints() {
        return groupAPoints;
    }

    public void setGroupAPoints(Integer groupAPoints) {
        this.groupAPoints = groupAPoints;
    }

    public Integer getGroupBPoints() {
        return groupBPoints;
    }

    public void setGroupBPoints(Integer groupBPoints) {
        this.groupBPoints = groupBPoints;
    }

    public Integer getGroupCPoints() {
        return groupCPoints;
    }

    public void setGroupCPoints(Integer groupCPoints) {
        this.groupCPoints = groupCPoints;
    }

    public Integer getPerfectGroupPhase() {
        return perfectGroupPhase;
    }

    public void setPerfectGroupPhase(Integer perfectGroupPhase) {
        this.perfectGroupPhase = perfectGroupPhase;
    }

    public Integer getQuarterFinalsPoints() {
        return quarterFinalsPoints;
    }

    public void setQuarterFinalsPoints(Integer quarterFinalsPoints) {
        this.quarterFinalsPoints = quarterFinalsPoints;
    }

    public Integer getSemiFinalsPoints() {
        return semiFinalsPoints;
    }

    public void setSemiFinalsPoints(Integer semiFinalsPoints) {
        this.semiFinalsPoints = semiFinalsPoints;
    }

    public Integer getFinalsPoints() {
        return finalsPoints;
    }

    public void setFinalsPoints(Integer finalsPoints) {
        this.finalsPoints = finalsPoints;
    }

    public Integer getPodiumPoints() {
        return podiumPoints;
    }

    public void setPodiumPoints(Integer podiumPoints) {
        this.podiumPoints = podiumPoints;
    }

    public Integer getGoldenBootPoints() {
        return goldenBootPoints;
    }

    public void setGoldenBootPoints(Integer goldenBootPoints) {
        this.goldenBootPoints = goldenBootPoints;
    }

    private Integer calculateGroup(Bet bet, String regex) {
        Integer result = 0;
        for (MatchResult matchResult : bet.getMatchResults()) {
            Match match = matchResult.getMatch();
            if (match.getIdentifier().matches(regex) && match.isPlayed()) {
                int realHomeGoals = match.getHomeGoals();
                int realAwayGoals = match.getAwayGoals();
                int betHomeGoals = matchResult.getHomeGoals();
                int betAwayGoals = matchResult.getAwayGoals();

                int realDiff = realHomeGoals - realAwayGoals;
                int betDiff = betHomeGoals - betAwayGoals;

                if ((betDiff > 0 && realDiff > 0) ||
                        (betDiff < 0 && realDiff < 0) ||
                        (betDiff == 0 && realDiff == 0)) {
                    result++;
                    if (realHomeGoals == betHomeGoals && realAwayGoals == betAwayGoals) result += 2;
                }
            }
        }
        if (result == 18) result += 12;
        return result;
    }

    private Integer calculateKnockOut(Bet bet, String regex) {
        Integer result = 0;
        int count = 0;
        for (MatchResult matchResult : bet.getMatchResults()) {
            Match match = matchResult.getMatch();
            if (match.getIdentifier().matches(regex)) {
                count++;
                if (match.isPlayed()) {
                    int realHomeGoals = match.getHomeGoals();
                    int realAwayGoals = match.getAwayGoals();
                    int betHomeGoals = matchResult.getHomeGoals();
                    int betAwayGoals = matchResult.getAwayGoals();

                    int realHomePenalties = match.getHomePenalties();
                    int realAwayPenalties = match.getAwayPenalties();
                    int betHomePenalties = matchResult.getHomePenalties();
                    int betAwayPenalties = matchResult.getAwayPenalties();

                    int realDiff = realHomeGoals - realAwayGoals;
                    int betDiff = betHomeGoals - betAwayGoals;
                    int realPenatiesDiff = realHomePenalties - realAwayPenalties;
                    int betPenatiesDiff = betHomePenalties - betAwayPenalties;

                    if ((betDiff > 0 && realDiff > 0) || (betDiff < 0 && realDiff < 0) ||
                            ((betDiff == 0 && realDiff == 0) &&
                                    ((betPenatiesDiff > 0 && realPenatiesDiff > 0) ||
                                            (betPenatiesDiff < 0 && realPenatiesDiff < 0)))) {
                        result++;
                        if (realHomeGoals == betHomeGoals && realAwayGoals == betAwayGoals &&
                                realHomePenalties == betHomePenalties && realAwayPenalties == betAwayPenalties)
                            result += 2;
                    }
                }
            }
        }
        if (result == (count * 3)) result += count * 2;
        return result;
    }

    private Integer calculateGoldenBoot(Bet bet) {
        Integer result = 0;
        List<Player> topScorers = goalService.getGoldenBootAsPlayerList();
        if (topScorers.contains(bet.getGoldenBoot())) result = 50;
        return result;
    }

    private Integer calculatePodium(Bet bet) {
        Integer result = 0;
        Match finalMatch = null;
        Match thirdPlaceMatch = null;

        for (MatchResult matchResult : bet.getMatchResults()) {
            Match match = matchResult.getMatch();
            if (match.getIdentifier().matches("^f1$") && match.isPlayed())
                finalMatch = match;
            if (match.getIdentifier().matches("^f2$") && match.isPlayed())
                thirdPlaceMatch = match;
        }

        if (finalMatch != null && thirdPlaceMatch != null) {
            Team firstPlace = finalMatch.calculateWinner().get();
            Team secondPlace = finalMatch.calculateLoser();
            Team thirdPlace = thirdPlaceMatch.calculateWinner().get();

            if (bet.getPodium().contains(firstPlace) &&
                    bet.getPodium().contains(secondPlace) &&
                    bet.getPodium().contains(thirdPlace))
                result = 30;

            if (bet.getFirstPlace().equals(firstPlace) &&
                    bet.getSecondPlace().equals(secondPlace) &&
                    bet.getThirdPlace().equals(thirdPlace))
                result += 30;
        }
        return result;
    }
}