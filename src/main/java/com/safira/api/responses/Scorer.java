package com.safira.api.responses;

/**
 * Created by developer on 09/06/15.
 */
public class Scorer {
    private String name;
    private Long amount;

    public Scorer() {
    }

    public Scorer(String name, Long amount) {
        this.name = name;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}
