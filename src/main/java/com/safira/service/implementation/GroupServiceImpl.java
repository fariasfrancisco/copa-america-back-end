package com.safira.service.implementation;

import com.safira.api.requests.MatchResponses;
import com.safira.api.responses.ErrorOutput;
import com.safira.api.responses.MatchResponse;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.Group;
import com.safira.domain.entities.Match;
import com.safira.domain.entities.Team;
import com.safira.service.interfaces.GroupService;
import com.safira.service.interfaces.MatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service("groupService")
public class GroupServiceImpl implements GroupService {
    @Autowired
    private MatchService matchService;

    @Transactional
    public Group buildGroup(MatchResponses responses, ErrorOutput errors) throws EmptyQueryResultException {
        Team[] teams;
        List<Team> teamList = new ArrayList<>();
        for (MatchResponse matchResponse : responses.getMatchResponses()) {
            Match match = matchService.getMatchByIdentifier(matchResponse.getIdentifier(), errors);
            Team homeTeam = match.getHomeTeam();
            Team awayTeam = match.getAwayTeam();
            if (!teamList.contains(homeTeam)) teamList.add(homeTeam);
            if (!teamList.contains(awayTeam)) teamList.add(awayTeam);
        }

        Map<String, Team> map = new LinkedHashMap<>();
        for (Team team : teamList) map.put(team.getName(), team);

        teamList.clear();
        teamList.addAll(map.values());

        int length = teamList.size();
        teams = new Team[length];
        for (int i = 0; i < length; i++) teams[i] = teamList.get(i);

        return new Group(responses, teams);
    }
}
