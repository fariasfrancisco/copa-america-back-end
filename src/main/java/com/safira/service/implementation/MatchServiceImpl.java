package com.safira.service.implementation;

import com.safira.api.requests.*;
import com.safira.api.responses.ErrorOutput;
import com.safira.api.responses.MatchResponse;
import com.safira.common.SafiraUtils;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.Group;
import com.safira.domain.TeamStats;
import com.safira.domain.entities.Goal;
import com.safira.domain.entities.Match;
import com.safira.domain.entities.Stage;
import com.safira.domain.entities.Team;
import com.safira.service.interfaces.GoalService;
import com.safira.service.interfaces.MatchService;
import com.safira.service.interfaces.StageService;
import com.safira.service.interfaces.TeamService;
import com.safira.service.repositories.MatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static com.safira.common.Regex.GROUP_STAGE_REGEX;

@Service("matchService")
public class MatchServiceImpl implements MatchService {

    @Autowired
    private MatchRepository matchRepository;

    @Autowired
    private TeamService teamService;

    @Autowired
    private StageService stageService;

    @Autowired
    private GoalService goalService;

    @Transactional
    public List<MatchResponse> registerMatches(RegisterMatchesRequest requests, ErrorOutput errors) throws EmptyQueryResultException {
        List<MatchResponse> matchResponses = new ArrayList<>();
        MatchResponse matchResponse;
        for (RegisterMatchRequest request : requests.getRegisterMatchRequests()) {
            matchResponse = registerMatch(request, errors);
            matchResponses.add(matchResponse);
        }
        return matchResponses;
    }

    @Transactional
    public MatchResponse registerMatch(RegisterMatchRequest request, ErrorOutput errors) throws EmptyQueryResultException {
        Team homeTeam = null;
        Team awayTeam = null;

        if (!request.getHomeTeamName().equals("null")) {
            homeTeam = teamService.getTeamByName(request.getHomeTeamName(), errors);
            if (SafiraUtils.isNull(homeTeam) && request.getIdentifier().matches(GROUP_STAGE_REGEX))
                errors.addError("Empty Query Exception", "homeTeamName", "No team was found with homeTeamName");
        }

        if (!request.getAwayTeamName().equals("null")) {
            awayTeam = teamService.getTeamByName(request.getAwayTeamName(), errors);
            if (SafiraUtils.isNull(awayTeam) && request.getIdentifier().matches(GROUP_STAGE_REGEX))
                errors.addError("Empty Query Exception", "awayTeamName", "No team was found with awayTeamName");
        }

        Stage stage = stageService.getStageByName(request.getStageName(), errors);
        if (SafiraUtils.isNull(stage))
            errors.addError("Empty Query Exception", "stageName", "No stage found with stageName");

        if (errors.hasErrors()) throw new EmptyQueryResultException();

        Match match = new Match.Builder()
                .withDate(request.getDateAsLocalDateTime())
                .withHomeTeam(homeTeam)
                .withAwayTeam(awayTeam)
                .withidentifier(request.getIdentifier())
                .withStage(stage)
                .build();
        matchRepository.save(match);
        return new MatchResponse(match);
    }

    @Transactional
    public MatchResponse updateMatch(UpdateMatchRequest request, ErrorOutput errors) throws EmptyQueryResultException {
        MatchResponse matchResponse;
        Match match = matchRepository.findByIdentifier(request.getIdentifier());
        if (SafiraUtils.isNull(match)) {
            errors.addError("EmptyQueryResult", "identifier", "No match found with identifier.");
            throw new EmptyQueryResultException();
        }
        List<Goal> goals = goalService.RegisterGoals(match,
                request.getGoalScorersHome(),
                request.getGoalScorersAway(),
                errors);
        match.setGoals(goals);
        if (request.getHomePenalties() != 0 || request.getAwayPenalties() != 0) {
            match.setHomePenalties(request.getHomePenalties());
            match.setAwayPenalties(request.getAwayPenalties());
        }
        matchRepository.save(match);
        updateBrackets(match, errors);

        matchResponse = new MatchResponse(match);
        return matchResponse;
    }

    @Override
    public List<MatchResponse> updateMatches(UpdateMatchesRequest request, ErrorOutput errors) throws EmptyQueryResultException {
        List<MatchResponse> matchResponses = new ArrayList<>();
        MatchResponse matchResponse;
        for (UpdateMatchRequest updateMatchRequest : request.getUpdateMatchRequests()) {
            matchResponse = updateMatch(updateMatchRequest, errors);
            matchResponses.add(matchResponse);
        }
        return matchResponses;
    }

    @Transactional
    public List<MatchResponse> getMatchByStage(String stageName, ErrorOutput errors) throws EmptyQueryResultException {
        Stage.StageName stage = Stage.StageName.valueOf(stageName);
        List<Match> matches = matchRepository.findByStage_StageName(stage);
        if (matches.isEmpty()) {
            errors.addError("EmptyQueryResult", "stageName", "No stage found with given name.");
            throw new EmptyQueryResultException();
        }
        List<MatchResponse> matchResponses = new ArrayList<>();
        for (Match match : matches) matchResponses.add(new MatchResponse(match));
        return matchResponses;
    }

    @Override
    public Match getMatchByIdentifier(String identifier, ErrorOutput errors) throws EmptyQueryResultException {
        Match match = matchRepository.findByIdentifier(identifier);
        if (SafiraUtils.isNull(match)) {
            errors.addError("EmptyQueryResult", "identifier", "No match found with given identifier.");
            throw new EmptyQueryResultException();
        }
        return match;
    }

    private void updateBrackets(Match match, ErrorOutput errors) throws EmptyQueryResultException {
        switch (match.getIdentifier()) {
            case "ga6": {
                Stage stage = match.getStage();
                List<Match> matches = SafiraUtils.toList(stage.getMatches());
                Group groupA = new Group(matches);
                updateBrackets("q1", groupA.getFirstPlace(), null, errors);
                updateBrackets("q2", groupA.getSecondPlace(), null, errors);
                return;
            }
            case "gb6": {
                Stage stage = match.getStage();
                List<Match> matches = SafiraUtils.toList(stage.getMatches());
                Group groupB = new Group(matches);
                updateBrackets("q3", groupB.getFirstPlace(), null, errors);
                updateBrackets("q4", null, groupB.getSecondPlace(), errors);
                return;
            }
            case "gc6": {
                Stage stage = match.getStage();
                List<Match> matches = SafiraUtils.toList(stage.getMatches());
                Group groupC = new Group(matches);
                updateBrackets("q4", groupC.getFirstPlace(), null, errors);
                updateBrackets("q2", null, groupC.getSecondPlace(), errors);
                determineBestThirdPlaces(errors);
                return;
            }
            case "q1": {
                Team homeTeam = match.calculateWinner().get();
                updateBrackets("s1", homeTeam, null, errors);
                return;
            }
            case "q2": {
                Team awayTeam = match.calculateWinner().get();
                updateBrackets("s1", null, awayTeam, errors);
                return;
            }
            case "q3": {
                Team homeTeam = match.calculateWinner().get();
                updateBrackets("s2", homeTeam, null, errors);
                return;
            }
            case "q4": {
                Team awayTeam = match.calculateWinner().get();
                updateBrackets("s2", null, awayTeam, errors);
                return;
            }
            case "s1": {
                updateBrackets("f2", match.calculateWinner().get(), null, errors);
                updateBrackets("f1", match.calculateLoser(), null, errors);
                return;
            }
            case "s2": {
                updateBrackets("f2", null, match.calculateWinner().get(), errors);
                updateBrackets("f1", null, match.calculateLoser(), errors);
            }
        }
    }

    private void determineBestThirdPlaces(ErrorOutput errors) throws EmptyQueryResultException {
        Stage stageGroupA = stageService.getStageByName("GROUP_A", errors);
        Stage stageGroupB = stageService.getStageByName("GROUP_B", errors);
        Stage stageGroupC = stageService.getStageByName("GROUP_C", errors);
        List<Match> groupAMatches = stageGroupA.getMatches();
        List<Match> groupBMatches = stageGroupB.getMatches();
        List<Match> groupCMatches = stageGroupC.getMatches();
        Group groupA = new Group(groupAMatches);
        Group groupB = new Group(groupBMatches);
        Group groupC = new Group(groupCMatches);

        ThirdPlaceTeams thirdPlaceTeams = new ThirdPlaceTeams();
        TeamStats teamStats;
        teamStats = new TeamStats(groupA.getThirdPlace(), groupA.getThirdPlaceStats());
        thirdPlaceTeams.getTeamStatsList().add(teamStats);
        teamStats = new TeamStats(groupB.getThirdPlace(), groupB.getThirdPlaceStats());
        thirdPlaceTeams.getTeamStatsList().add(teamStats);
        teamStats = new TeamStats(groupC.getThirdPlace(), groupC.getThirdPlaceStats());
        thirdPlaceTeams.getTeamStatsList().add(teamStats);
        Group thirdPlaceTeamsGroup = new Group(thirdPlaceTeams);
        Team bestThird = thirdPlaceTeamsGroup.getFirstPlace();
        Team secondToBestThird = thirdPlaceTeamsGroup.getSecondPlace();

        updateBrackets("q1", null, bestThird, errors);
        updateBrackets("q3", null, secondToBestThird, errors);
    }

    @Transactional
    private void updateBrackets(String identifier, Team homeTeam, Team awayTeam, ErrorOutput errors) throws EmptyQueryResultException {
        Match match = matchRepository.findByIdentifier(identifier);
        if (SafiraUtils.isNull(match)) {
            errors.addError("EmptyQueryResult", "identifier", "No match found with given identifier.");
            throw new EmptyQueryResultException();
        }
        if (SafiraUtils.isNull(match.getHomeTeam()))
            match.setHomeTeam(homeTeam);
        if (SafiraUtils.isNull(match.getAwayTeam()))
            match.setAwayTeam(awayTeam);
        matchRepository.save(match);
    }
}
