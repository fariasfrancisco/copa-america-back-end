package com.safira.service.implementation;

import com.safira.api.requests.RegisterPlayerRequest;
import com.safira.api.requests.RegisterPlayersRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.SafiraUtils;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.entities.Player;
import com.safira.domain.entities.Team;
import com.safira.service.interfaces.PlayerService;
import com.safira.service.interfaces.TeamService;
import com.safira.service.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service("playerService")
public class PlayerServiceImpl implements PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    @Autowired
    private TeamService teamService;

    @Transactional
    public List<Player> registerPlayers(RegisterPlayersRequest requests, ErrorOutput errors) throws EmptyQueryResultException {
        List<Player> players = new ArrayList<>();
        Player player;
        for (RegisterPlayerRequest request : requests.getRegisterPlayerRequests()) {
            player = registerPlayer(request, errors);
            players.add(player);
        }
        return players;
    }

    @Transactional
    public Player registerPlayer(RegisterPlayerRequest request, ErrorOutput errors) throws EmptyQueryResultException {
        Team team = teamService.getTeamByName(request.getTeamName(), errors);
        if (SafiraUtils.isNull(team)) {
            errors.addError("EmptyQueryException", "teamName", "No team found with teamName.");
            throw new EmptyQueryResultException();
        }
        Player player = new Player.Builder()
                .withFullName(request.getFullName())
                .withTeam(team)
                .build();
        playerRepository.save(player);
        return player;
    }

    @Transactional
    public List<Player> getPlayersByTeam(String name, ErrorOutput errors) throws EmptyQueryResultException {
        List<Player> players = playerRepository.findByTeam_Name(name);
        if (players.isEmpty()) {
            errors.addError("EmptyQueryException", "teamName", "No team found with teamName.");
            throw new EmptyQueryResultException();
        }
        return players;
    }

    @Transactional
    public List<Player> getPlayersByName(String fullName, ErrorOutput errors) throws EmptyQueryResultException {
        List<Player> players = playerRepository.findByFullNameContainingIgnoreCase(fullName);
        if (players.isEmpty()) {
            errors.addError("EmptyQueryException", "teamName", "No players found with fullName.");
            throw new EmptyQueryResultException();
        }
        return players;
    }
}
