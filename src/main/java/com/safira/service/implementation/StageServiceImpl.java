package com.safira.service.implementation;

import com.safira.api.requests.LoadStagesRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.StageName;
import com.safira.domain.entities.Stage;
import com.safira.service.interfaces.StageService;
import com.safira.service.repositories.StageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service("stageService")
public class StageServiceImpl implements StageService {
    @Autowired
    private StageRepository stageRepository;

    @Transactional
    public List<Stage> loadStages(LoadStagesRequest request, ErrorOutput errors) {
        Stage stage;
        List<Stage> stages = new ArrayList<>();
        for (StageName stageName : request.getStageNames()) {
            Stage.StageName stgnm = Stage.StageName.valueOf(stageName.getStageName());
            stage = new Stage.Builder()
                    .withStageName(stgnm)
                    .build();
            stageRepository.save(stage);
            stages.add(stage);
        }
        return stages;
    }

    @Transactional
    public Stage getStageByName(String stageName, ErrorOutput errors) {
        //TODO write validation
        Stage.StageName stgnm = Stage.StageName.valueOf(stageName);
        return stageRepository.findByStageName(stgnm);
    }
}
