package com.safira.service.implementation;

import com.safira.api.requests.LoginBetRequest;
import com.safira.api.requests.RegisterAccountRequest;
import com.safira.api.requests.RegisterBetRequest;
import com.safira.api.responses.BetPoints;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.Result;
import com.safira.common.SafiraUtils;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.common.exceptions.LoginException;
import com.safira.common.exceptions.ValidatorException;
import com.safira.domain.entities.*;
import com.safira.service.PasswordService;
import com.safira.service.interfaces.BetService;
import com.safira.service.interfaces.MatchService;
import com.safira.service.interfaces.TeamService;
import com.safira.service.repositories.BetAccountRepository;
import com.safira.service.repositories.BetRepository;
import com.safira.service.repositories.MatchResultRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service("betService")
public class BetServiceImpl implements BetService {
    @Autowired
    private BetRepository betRepository;

    @Autowired
    private BetAccountRepository betAccountRepository;

    @Autowired
    private MatchResultRepository matchResultRepository;

    @Autowired
    private TeamService teamService;

    @Autowired
    private MatchService matchService;

    @Transactional
    public BetAccount registerAccount(RegisterAccountRequest request, ErrorOutput errors) {
        byte[] salt = PasswordService.getNextSalt();
        char[] password = request.getPassword().toCharArray();
        byte[] hash = PasswordService.hash(password, salt);

        BetAccount betAccount = new BetAccount.Builder()
                .withUsername(request.getUsername())
                .withHash(hash)
                .withSalt(salt)
                .build();

        betAccountRepository.save(betAccount);
        return betAccount;
    }

    @Transactional
    public Bet registerBet(RegisterBetRequest request, ErrorOutput errors) throws EmptyQueryResultException {
        BetAccount betAccount = betAccountRepository.findByUsername(request.getUsername());
        if (SafiraUtils.isNull(betAccount))
            errors.addError("EmptyQueryError", "username", "Could not find any Account with username.");

        Team team = teamService.getTeamByName(request.getGoldenBootTeam(), errors);
        Player goldenBoot = null;
        if (SafiraUtils.isNull(team))
            errors.addError("EmptyQueryError", "goldenBoot", "Could not find Player with goldenBoot.");
        else {
            for (Player player : team.getPlayers())
                if (player.getFullName().equals(request.getGoldenBootPlayer()))
                    goldenBoot = player;
            if (SafiraUtils.isNull(goldenBoot))
                errors.addError("EmptyQueryError", "goldenBoot", "Could not find Player with goldenBoot.");
        }

        Team firstPlace = teamService.getTeamByName(request.getFirstPlaceTeam(), errors);
        if (SafiraUtils.isNull(firstPlace))
            errors.addError("EmptyQueryError", "firstPlace", "Could not find Team with firstPlace.");

        Team secondPlace = teamService.getTeamByName(request.getSecondPlaceTeam(), errors);
        if (SafiraUtils.isNull(secondPlace))
            errors.addError("EmptyQueryError", "secondPlace", "Could not find Team with secondPlace.");

        Team thirdPlace = teamService.getTeamByName(request.getThirdPlaceTeam(), errors);
        if (SafiraUtils.isNull(thirdPlace))
            errors.addError("EmptyQueryError", "thirdPlace", "Could not find Team with thirdPlace.");

        if (errors.hasErrors()) throw new EmptyQueryResultException();

        Bet bet = new Bet.Builder()
                .withGolderBoot(goldenBoot)
                .withFirstPlace(firstPlace)
                .withSecondPlace(secondPlace)
                .withThirdPlace(thirdPlace)
                .build();

        Match match;
        for (Result result : request.getResults()) {
            match = matchService.getMatchByIdentifier(result.getIdentifier(), errors);
            MatchResult matchResult = new MatchResult(match,
                    bet,
                    result.getHomeGoals(),
                    result.getAwayGoals(),
                    result.getHomePenalties(),
                    result.getAwayPenalties());
            matchResultRepository.save(matchResult);
        }

        assert betAccount != null;
        betAccount.setBet(bet);
        betAccountRepository.save(betAccount);
        betRepository.save(bet);
        return bet;
    }

    @Transactional
    public BetAccount retrieveBetByUsername(LoginBetRequest request, ErrorOutput errors) throws LoginException {
        BetAccount bet = betAccountRepository.findByUsername(request.getUsername());
        if (SafiraUtils.isNull(bet)) {
            errors.addError("LoginException", "username", "No bet found with username.");
            throw new LoginException();
        }
        char[] password = request.getPassword().toCharArray();
        byte[] hash = bet.getHash();
        byte[] salt = bet.getSalt();
        if (!PasswordService.isExpectedPassword(password, salt, hash)) {
            errors.addError("LoginException", "username, password", "Failed to login with username/password.");
            throw new LoginException();
        }
        return bet;
    }

    @Transactional
    public BetAccount validateBet(String username, ErrorOutput errors) throws ValidatorException {
        BetAccount bet = betAccountRepository.findByUsername(username);
        if (SafiraUtils.isNull(bet)) {
            errors.addError("ValidationException", "username", "No bet found with username.");
            throw new ValidatorException();
        }
        bet.setValidated(true);
        betAccountRepository.save(bet);
        return bet;
    }

    @Transactional
    public List<BetPoints> retrieveAllBetPoints(ErrorOutput errors) throws EmptyQueryResultException {
        List<BetAccount> betAccounts = betAccountRepository.findAll();
        if (betAccounts.isEmpty()) {
            errors.addError("EmptyQueryException", "", "No accounts could be retrieved.");
            throw new EmptyQueryResultException();
        }
        List<BetPoints> betPoints = new ArrayList<>();
        for (BetAccount betAccount : betAccounts)
            betPoints.add(new BetPoints(betAccount));
        return betPoints;
    }
}
