package com.safira.service.implementation;

import com.safira.api.requests.RegisterTeamRequest;
import com.safira.api.requests.RegisterTeamsRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.SafiraUtils;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.entities.Team;
import com.safira.service.interfaces.TeamService;
import com.safira.service.repositories.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service("teamService")
public class TeamServiceImpl implements TeamService {

    @Autowired
    private TeamRepository teamRepository;

    @Override
    public List<Team> registerTeams(RegisterTeamsRequest requests, ErrorOutput errors) {
        List<Team> teams = new ArrayList<>();
        Team team;
        for (RegisterTeamRequest request : requests.getRegisterTeamRequests()) {
            team = registerTeam(request, errors);
            teams.add(team);
        }
        return teams;
    }

    @Transactional
    public Team registerTeam(RegisterTeamRequest request, ErrorOutput errors) {
        Team team = new Team.Builder()
                .withName(request.getName())
                .build();
        teamRepository.save(team);
        return team;
    }

    @Transactional
    public List<Team> getAllTeams(ErrorOutput errors) {
        return teamRepository.findAll();
    }

    @Transactional
    public Team getTeamByName(String name, ErrorOutput errors) throws EmptyQueryResultException {
        Team team = teamRepository.findByNameContainingIgnoreCase(name);
        if (SafiraUtils.isNull(team)) {
            errors.addError("EmptyQueryException", "teamName", "No team found with name.");
            throw new EmptyQueryResultException();
        }
        return team;
    }
}
