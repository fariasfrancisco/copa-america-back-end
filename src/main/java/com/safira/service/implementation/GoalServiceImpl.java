package com.safira.service.implementation;

import com.safira.api.responses.ErrorOutput;
import com.safira.api.responses.Scorer;
import com.safira.common.GoalScorer;
import com.safira.common.SafiraUtils;
import com.safira.domain.entities.Goal;
import com.safira.domain.entities.Match;
import com.safira.domain.entities.Player;
import com.safira.domain.entities.Team;
import com.safira.service.interfaces.GoalService;
import com.safira.service.repositories.GoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service("goalService")
public class GoalServiceImpl implements GoalService {
    @Autowired
    private GoalRepository goalRepository;

    @Transactional
    public List<Goal> RegisterGoals(Match match, List<GoalScorer> goalScorersHome, List<GoalScorer> goalScorersAway, ErrorOutput errors) {
        List<Goal> homeGoals = iterateAndSaveGoals(goalScorersHome, match.getHomeTeam(), match);
        List<Goal> awayGoals = iterateAndSaveGoals(goalScorersAway, match.getAwayTeam(), match);
        List<Goal> goals = new ArrayList<>();
        goals.addAll(homeGoals);
        goals.addAll(awayGoals);
        return goals;
    }

    @Transactional
    public List<Player> getGoldenBootAsPlayerList() {
        List<Goal> goals = goalRepository.findAll();
        List<Player> playerList = new ArrayList<>();
        List<Player> out = new ArrayList<>();
        Map<Player, Long> scorersMap = new HashMap<>();

        for (Goal goal : goals) playerList.add(goal.getPlayer());
        Set<Player> playerSet = SafiraUtils.toSet(playerList);
        for (Player player : playerSet) {
            Long goalsScored = goalRepository.countByPlayer(player);
            scorersMap.put(player, goalsScored);
        }

        for (Player player : playerSet) {
            if (out.isEmpty()) out.add(player);
            else {
                Player p = out.get(0);
                Long diff = scorersMap.get(p) - scorersMap.get(player);
                if (diff < 0) {
                    out.clear();
                    out.add(player);
                } else if (diff == 0) out.add(player);
            }
        }

        return out;
    }

    @Transactional
    public List<Scorer> getGoldenBootAsScorerList() {
        List<Scorer> scorers = new ArrayList<>();
        List<Goal> goals = goalRepository.findAll();
        List<Player> playerList = new ArrayList<>();
        List<Player> out = new ArrayList<>();
        Map<Player, Long> scorersMap = new HashMap<>();

        for (Goal goal : goals) playerList.add(goal.getPlayer());
        Set<Player> playerSet = SafiraUtils.toSet(playerList);

        for (Player player : playerSet) {
            Long goalsScored = goalRepository.countByPlayer(player);
            scorersMap.put(player, goalsScored);
        }

        for (Player player : playerSet) {
            if (out.isEmpty()) out.add(player);
            else {
                Player p = out.get(0);
                Long diff = scorersMap.get(p) - scorersMap.get(player);
                if (diff < 0) {
                    out.clear();
                    out.add(player);
                } else if (diff == 0) out.add(player);
            }
        }

        for (Player player : out) {
            Long amount = goalRepository.countByPlayer(player);
            scorers.add(new Scorer(player.getFullName(), amount));
        }

        return scorers;
    }

    @Transactional
    public List<Scorer> getTop5Scorers() {
        List<Scorer> scorers = new ArrayList<>();
        Map<Player, Long> scorersMap = new LinkedHashMap<>();
        List<Goal> goals = goalRepository.findAll();
        List<Long> goalList = new ArrayList<>();

        for (Goal goal : goals) {
            Player player = goal.getPlayer();
            Long amount = goalRepository.countByPlayer(player);
            goalList.add(amount);
            scorersMap.put(player, amount);
        }

        Collections.sort(goalList);
        int length = goalList.size() > 4 ? 5 : goalList.size();

        Long[] goalArray = new Long[length];

        for (int index = 0; index < length; index++)
            goalArray[index] = goalList.get(index);

        for (Object o : scorersMap.entrySet()) {
            Map.Entry pair = (Map.Entry) o;
            Long value = (Long) pair.getValue();
            Player key = (Player) pair.getKey();
            if (!SafiraUtils.contains(goalArray, value))
                scorersMap.remove(key);
            else scorers.add(new Scorer(key.getFullName(), value));
        }

        return scorers;
    }

    @Transactional
    private List<Goal> iterateAndSaveGoals(List<GoalScorer> goalsScorers, Team team, Match match) {
        List<Goal> goals = new ArrayList<>();
        for (GoalScorer goalScorer : goalsScorers)
            for (Player player : team.getPlayers())
                if (player.getFullName().equals(goalScorer.getName())) {
                    Goal goal = new Goal.Builder()
                            .withMatch(match)
                            .withPlayer(player)
                            .build();
                    goals.add(goal);
                    goalRepository.save(goal);
                }
        return goals;
    }
}
