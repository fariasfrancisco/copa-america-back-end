package com.safira.service.repositories;

import com.safira.domain.entities.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Long> {
    Team findByNameContainingIgnoreCase(String name);
}
