package com.safira.service.repositories;

import com.safira.domain.entities.Player;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Long> {
    List<Player> findByTeam_Name(String name);

    List<Player> findByFullNameContainingIgnoreCase(String fullName);
}
