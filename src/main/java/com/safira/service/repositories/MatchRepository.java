package com.safira.service.repositories;

import com.safira.domain.entities.Match;
import com.safira.domain.entities.Stage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MatchRepository extends JpaRepository<Match, Long> {
    List<Match> findByStage_StageName(Stage.StageName stageName);

    Match findByIdentifier(String identifier);
}
