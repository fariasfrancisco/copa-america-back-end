package com.safira.service.repositories;

import com.safira.domain.entities.BetAccount;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by developer on 05/06/15.
 */
public interface BetAccountRepository extends JpaRepository<BetAccount, Long> {
    BetAccount findByUsername(String username);
}
