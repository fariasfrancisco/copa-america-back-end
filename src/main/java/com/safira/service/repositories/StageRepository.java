package com.safira.service.repositories;

import com.safira.domain.entities.Stage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StageRepository extends JpaRepository<Stage, Long> {
    Stage findByStageName(Stage.StageName stageName);
}
