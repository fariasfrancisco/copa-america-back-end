package com.safira.service.repositories;

import com.safira.domain.entities.Goal;
import com.safira.domain.entities.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GoalRepository extends JpaRepository<Goal, Long> {
    Long countByPlayer(Player player);
}
