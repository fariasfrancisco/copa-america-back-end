package com.safira.service;

import com.safira.api.requests.RegisterMatchRequest;
import com.safira.api.requests.RegisterMatchesRequest;
import com.safira.api.requests.UpdateMatchRequest;
import com.safira.api.requests.UpdateMatchesRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.SafiraUtils;
import com.safira.common.exceptions.ValidatorException;
import com.safira.domain.entities.Stage;

import static com.safira.common.Regex.GROUP_STAGE_REGEX;

public class Validator {

    private static final String VALID_IDENTIFIER = "^(g[a-c][1-6]|q[1-4]|s[1-2]|f[1-2])$";

    private static final String[] GROUP_A_TEAMS = new String[]{"Chile", "México", "Ecuador", "Bolivia"};

    private static final String[] GROUP_B_TEAMS = new String[]{"Argentina", "Uruguay", "Jamaica", "Paraguay"};

    private static final String[] GROUP_C_TEAMS = new String[]{"Brasil", "Colombia", "Perú ", "Venezuela "};

    public static void validateMatchRequest(RegisterMatchRequest request, ErrorOutput errors) throws ValidatorException {
        if (!validateIdentifier(request.getIdentifier()))
            errors.addError("ValidationException", "identifier", "Recieved identifier does not match any pattern.");
        if (!validateStageAndIdentifier(request.getStageName(), request.getIdentifier()))
            errors.addError("ValidationException", "identifier, stageName", "Recieved identifier does not match stageName.");
        if (errors.hasErrors()) throw new ValidatorException();
    }

    public static void validateMatchRequests(RegisterMatchesRequest requests, ErrorOutput errors) throws ValidatorException {
        for (RegisterMatchRequest request : requests.getRegisterMatchRequests())
            validateMatchRequest(request, errors);
    }

    public static void validateUpdateMatchRequest(UpdateMatchRequest request, ErrorOutput errors) throws ValidatorException {
        if (!validateResult(request))
            errors.addError("ValidationException", "updateRequest", "Recieved result is not valid");
        if (errors.hasErrors()) throw new ValidatorException();
    }

    public static void validateUpdateMatchesRequest(UpdateMatchesRequest request, ErrorOutput errors) throws ValidatorException {
        for (UpdateMatchRequest updateMatchRequest : request.getUpdateMatchRequests())
            validateUpdateMatchRequest(updateMatchRequest, errors);
    }

    private static boolean validateResult(UpdateMatchRequest request) {
        Integer homeGoals = request.getGoalScorersHome().size();
        Integer awayGoals = request.getGoalScorersAway().size();
        Integer goalDiff = homeGoals - awayGoals;
        Integer penaltyDiff = request.getHomePenalties() - request.getAwayPenalties();
        if ((goalDiff > 0 || goalDiff < 0) && penaltyDiff != 0)
            return false;
        else if (goalDiff == 0)
            if (!request.getIdentifier().matches(GROUP_STAGE_REGEX) && penaltyDiff == 0)
                return false;
            else if (request.getIdentifier().matches(GROUP_STAGE_REGEX) && penaltyDiff != 0)
                return false;
        return true;
    }

    private static boolean validateIdentifier(String identifier) {
        return identifier.matches(VALID_IDENTIFIER);
    }

    private static boolean validateStageAndIdentifier(String stageName, String identifier) {
        switch (identifier) {
            case "ga1":
            case "ga2":
            case "ga3":
            case "ga4":
            case "ga5":
            case "ga6": {
                return Stage.StageName.valueOf(stageName).equals(Stage.StageName.GROUP_A);
            }
            case "gb1":
            case "gb2":
            case "gb3":
            case "gb4":
            case "gb5":
            case "gb6": {
                return Stage.StageName.valueOf(stageName).equals(Stage.StageName.GROUP_B);
            }
            case "gc1":
            case "gc2":
            case "gc3":
            case "gc4":
            case "gc5":
            case "gc6": {
                return Stage.StageName.valueOf(stageName).equals(Stage.StageName.GROUP_C);
            }
            case "q1":
            case "q2":
            case "q3":
            case "q4": {
                return Stage.StageName.valueOf(stageName).equals(Stage.StageName.QUARTER_FINALS);
            }
            case "s1":
            case "s2": {
                return Stage.StageName.valueOf(stageName).equals(Stage.StageName.SEMI_FINALS);
            }
            case "f1":
            case "f2": {
                return Stage.StageName.valueOf(stageName).equals(Stage.StageName.FINALS);
            }
        }
        return false;
    }

    private static boolean validateStageAndTeams(String stageName, String homeTeam, String awayTeam) {
        if (Stage.StageName.GROUP_A.equals(Stage.StageName.valueOf(stageName)) &&
                (!SafiraUtils.toList(GROUP_A_TEAMS).contains(homeTeam) ||
                        !SafiraUtils.toList(GROUP_A_TEAMS).contains(awayTeam))) return false;
        if (Stage.StageName.GROUP_B.equals(Stage.StageName.valueOf(stageName)) &&
                (!SafiraUtils.toList(GROUP_B_TEAMS).contains(homeTeam) ||
                        !SafiraUtils.toList(GROUP_B_TEAMS).contains(awayTeam))) return false;
        if (Stage.StageName.GROUP_C.equals(Stage.StageName.valueOf(stageName)) &&
                (!SafiraUtils.toList(GROUP_C_TEAMS).contains(homeTeam) ||
                        !SafiraUtils.toList(GROUP_C_TEAMS).contains(awayTeam))) return false;
        return true;
    }
}
