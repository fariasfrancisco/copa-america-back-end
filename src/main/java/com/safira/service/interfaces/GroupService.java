package com.safira.service.interfaces;

import com.safira.api.requests.MatchResponses;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.Group;

public interface GroupService {
    Group buildGroup(MatchResponses responses, ErrorOutput errors) throws EmptyQueryResultException;
}
