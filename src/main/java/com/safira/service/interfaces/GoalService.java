package com.safira.service.interfaces;

import com.safira.api.responses.ErrorOutput;
import com.safira.api.responses.Scorer;
import com.safira.common.GoalScorer;
import com.safira.domain.entities.Goal;
import com.safira.domain.entities.Match;
import com.safira.domain.entities.Player;

import java.util.List;

public interface GoalService {
    List<Goal> RegisterGoals(Match match,
                             List<GoalScorer> goalScorersHome,
                             List<GoalScorer> goalScorersAway,
                             ErrorOutput errors);

    List<Scorer> getTop5Scorers();

    List<Player> getGoldenBootAsPlayerList();

    List<Scorer> getGoldenBootAsScorerList();
}
