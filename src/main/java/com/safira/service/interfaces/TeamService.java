package com.safira.service.interfaces;

import com.safira.api.requests.RegisterTeamRequest;
import com.safira.api.requests.RegisterTeamsRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.entities.Team;

import java.util.List;

public interface TeamService {
    List<Team> registerTeams(RegisterTeamsRequest requests, ErrorOutput errors);

    Team registerTeam(RegisterTeamRequest request, ErrorOutput errors);

    List<Team> getAllTeams(ErrorOutput errors);

    Team getTeamByName(String name, ErrorOutput errors) throws EmptyQueryResultException;
}
