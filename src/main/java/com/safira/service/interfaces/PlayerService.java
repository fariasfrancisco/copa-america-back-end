package com.safira.service.interfaces;

import com.safira.api.requests.RegisterPlayerRequest;
import com.safira.api.requests.RegisterPlayersRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.entities.Player;

import java.util.List;

public interface PlayerService {
    List<Player> registerPlayers(RegisterPlayersRequest request, ErrorOutput errors) throws EmptyQueryResultException;

    Player registerPlayer(RegisterPlayerRequest request, ErrorOutput errors) throws EmptyQueryResultException;

    List<Player> getPlayersByTeam(String name, ErrorOutput errors) throws EmptyQueryResultException;

    List<Player> getPlayersByName(String name, ErrorOutput errors) throws EmptyQueryResultException;
}
