package com.safira.service.interfaces;

import com.safira.api.requests.RegisterMatchRequest;
import com.safira.api.requests.RegisterMatchesRequest;
import com.safira.api.requests.UpdateMatchRequest;
import com.safira.api.requests.UpdateMatchesRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.api.responses.MatchResponse;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.domain.entities.Match;

import java.util.List;

public interface MatchService {
    MatchResponse registerMatch(RegisterMatchRequest registerMatchRequest, ErrorOutput errors) throws EmptyQueryResultException;

    List<MatchResponse> registerMatches(RegisterMatchesRequest request, ErrorOutput errors) throws EmptyQueryResultException;

    MatchResponse updateMatch(UpdateMatchRequest updateMatchRequest, ErrorOutput errors) throws EmptyQueryResultException;

    List<MatchResponse> updateMatches(UpdateMatchesRequest request, ErrorOutput errors) throws EmptyQueryResultException;

    List<MatchResponse> getMatchByStage(String stageName, ErrorOutput errors) throws EmptyQueryResultException;

    Match getMatchByIdentifier(String identifier, ErrorOutput errors) throws EmptyQueryResultException;
}
