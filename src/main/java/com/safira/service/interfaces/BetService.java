package com.safira.service.interfaces;

import com.safira.api.requests.LoginBetRequest;
import com.safira.api.requests.RegisterAccountRequest;
import com.safira.api.requests.RegisterBetRequest;
import com.safira.api.responses.BetPoints;
import com.safira.api.responses.ErrorOutput;
import com.safira.common.exceptions.EmptyQueryResultException;
import com.safira.common.exceptions.LoginException;
import com.safira.common.exceptions.ValidatorException;
import com.safira.domain.entities.Bet;
import com.safira.domain.entities.BetAccount;

import java.util.List;

public interface BetService {

    BetAccount registerAccount(RegisterAccountRequest request, ErrorOutput errors);

    Bet registerBet(RegisterBetRequest request, ErrorOutput errors) throws EmptyQueryResultException;

    BetAccount retrieveBetByUsername(LoginBetRequest request, ErrorOutput errors) throws LoginException;

    BetAccount validateBet(String username, ErrorOutput errors) throws ValidatorException;

    List<BetPoints> retrieveAllBetPoints(ErrorOutput errors) throws EmptyQueryResultException;
}
