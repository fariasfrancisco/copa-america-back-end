package com.safira.service.interfaces;

import com.safira.api.requests.LoadStagesRequest;
import com.safira.api.responses.ErrorOutput;
import com.safira.domain.entities.Stage;

import java.util.List;

public interface StageService {
    List<Stage> loadStages(LoadStagesRequest request, ErrorOutput errors);

    Stage getStageByName(String stageName, ErrorOutput errors);
}
